#flavio
import flavio
from flavio.statistics.probability import NormalDistribution

#other libraries
import numpy as np
from util import *
import time 
import sys
import pickle

def main():
    #check first whether toy fit or data fit from the suffix that is used to store the fit results
    smear, use_SM_pred = None, None
    if 'toy' in suffix:
        print('Suffix contains toy')
        smear = True
        use_SM_pred = True
    elif 'data' in suffix:
        print('Suffix contains data')
        smear = False
        use_SM_pred = False
    else:
        raise Exception('Suffix {0} not recognised. Please check!'.format(suffix))

    #get the small rs that are floated
    floated_small_rs = [fp for fp in list(fit_parameters.keys()) if fp in small_rparams]

    #make SM toys for predefined observables
    print('Adding observables')
    all_obs_dict  = {} 
    all_obs_dict.update(add_observables('RK', toy_index, smear = smear, use_SM_pred = use_SM_pred))
    all_obs_dict.update(add_observables('RKst', toy_index, smear = smear, use_SM_pred = use_SM_pred))
    all_obs_dict.update(add_observables('BsTomumu', toy_index, smear = smear, use_SM_pred = use_SM_pred))
    all_obs_dict.update(add_observables('B0ToKstmumu', toy_index, smear = smear, use_SM_pred = use_SM_pred))
    if include_rx:
        rpK_lhcb_central_val   = False
        rpKerr_scaledown_run12 = True
        print('Adding RX observables')
        if 'data' in suffix: print('Whether central value of RpK the lhcb result?', rpK_lhcb_central_val)
        print('Whether scaling RpK error to full run1+run2?', rpKerr_scaledown_run12)
        #create new rx observales and make SM toys for those
        make_newparameters(fit_parameters, floated_small_rs)
        obs_list = ['<RpK>(Lb->pKll)', '<RKpi>(B0->Kpill)', '<RKpipi>(B+->Kpipill)']
        for obs_name in obs_list:
            make_newobservables(obs_name)
            all_obs_dict.update(add_observables(obs_name[1:].split('>')[0], toy_index, smear = smear, use_SM_pred = use_SM_pred, rpK_lhcb_central_val = rpK_lhcb_central_val, rpKerr_scaledown_run12 = rpKerr_scaledown_run12))

        #add gaussian constraint on small r's
        add_constraints(constrained_fit_params)

    #make a list of names of all measurement and all observables and define the likelihood
    measurement_names= list(all_obs_dict.keys())
    all_obs_list = [a_obs_j for a_obs_i in list(all_obs_dict.values()) for a_obs_j in a_obs_i]
    #print(measurement_names)
    #print(all_obs_list)
    likelihood = Likelihood(include_measurements=measurement_names, observables=all_obs_list, fit_parameters=list(constrained_fit_params.keys()))

    #do the minimisation
    floated_small_rs = [fp for fp in list(fit_parameters.keys()) if fp in small_rparams]
    #params = {fp: fit_parameters[fp].value for fp in fit_parameters}
    #print(cost_func_rx(params, (likelihood, floated_small_rs)))
    #exit(1)

    results = Minimize(fit_parameters, cost_func_rx, args=(likelihood, floated_small_rs), methods=['MIGRAD', 'SLSQP'], nfits = 1, use_hesse = False, use_minos = False, get_covariance = False, randomise = False, disp = True)
    print(results)

    fname   = '/home/hep/amathad/Packages/flavio-globalwcfit-rx-inclusion/fitresults_RXWideRange/result_'+str(toy_index)+'_'+combination_type+'_rx'+str(include_rx)+'_'+suffix+'.p'
    pickle.dump(results, open( fname, "wb" ) )

    return results

if __name__ == '__main__':
    start = time.time()

    #set few initial things
    toy_index        = int(sys.argv[1])
    combination_type = sys.argv[2]
    include_rx       = eval(sys.argv[3])
    suffix           = sys.argv[4]
    print('Parameters passed into script are:')
    print('Toy index:'      , toy_index)
    print('Fit combination:', combination_type)
    print('Include RX:'     , include_rx)
    print('Suffix:'         , suffix)

    #set floated params
    fit_parameters = {}

    #add names of constrained fit parameters (specially small r's)
    constrained_fit_params = {}

    #small r's for pK, Kpi and Kpipi
    if include_rx:
        #Parameter limits where central value is mid-point of range
        fit_parameters['rL_pK']        = FitParameter('rL_pK'       , 0.0, -2.0, 2.0,0.01)
        fit_parameters['rL_Kpi']       = FitParameter('rL_Kpi'      , 0.0, -2.0, 2.0,0.01)
        fit_parameters['rL_Kpipi']     = FitParameter('rL_Kpipi'    , 0.0, -2.0, 2.0,0.01)
        fit_parameters['r7_pK']        = FitParameter('r7_pK'       ,80.0,  0.0,160.0,0.01)
        fit_parameters['r7_Kpi']       = FitParameter('r7_Kpi'      ,50.0,  0.0,100.0,0.01)
        fit_parameters['r7_Kpipi']     = FitParameter('r7_Kpipi'    ,50.0,  0.0,100.0,0.01)
        fit_parameters['r1_pK']        = FitParameter('r1_pK'       , 0.0, -20.0,20.0, 0.01)
        fit_parameters['r1_Kpi']       = FitParameter('r1_Kpi'      , 0.0, -20.0,20.0, 0.01)
        fit_parameters['r1_Kpipi']     = FitParameter('r1_Kpipi'    , 0.0, -20.0,20.0, 0.01)
        fit_parameters['r2_pK']        = FitParameter('r2_pK'       , 0.0, -10.0,10.0, 0.01)
        fit_parameters['r2_Kpi']       = FitParameter('r2_Kpi'      , 0.0, -10.0,10.0, 0.01)
        fit_parameters['r2_Kpipi']     = FitParameter('r2_Kpipi'    , 0.0, -10.0,10.0, 0.01)

        ##constrain rL's: mu = central value that is mid-point of range and sigma = full range width.
        constrained_fit_params['rL_pK']           = NormalDistribution( 0.0, 4.0) #TruncatedNormalDistribution(0.0, 4.0, -2.0 , 2.0)
        constrained_fit_params['rL_Kpi']          = NormalDistribution( 0.0, 4.0) #TruncatedNormalDistribution(0.0, 4.0, -2.0 , 2.0)
        constrained_fit_params['rL_Kpipi']        = NormalDistribution( 0.0, 4.0) #TruncatedNormalDistribution(0.0, 4.0, -2.0 , 2.0)
        constrained_fit_params['r7_pK']           = NormalDistribution(80.0,160.0)
        constrained_fit_params['r7_Kpi']          = NormalDistribution(50.0,100.0)
        constrained_fit_params['r7_Kpipi']        = NormalDistribution(50.0,100.0)
        constrained_fit_params['r1_pK']           = NormalDistribution(0.0, 40.0)
        constrained_fit_params['r1_Kpi']          = NormalDistribution(0.0, 40.0)
        constrained_fit_params['r1_Kpipi']        = NormalDistribution(0.0, 40.0)
        constrained_fit_params['r2_pK']           = NormalDistribution(0.0, 20.0)
        constrained_fit_params['r2_Kpi']          = NormalDistribution(0.0, 20.0)
        constrained_fit_params['r2_Kpipi']        = NormalDistribution(0.0, 20.0)

    fit_parameters['C9mu'        ] = FitParameter('C9mu'        ,0.0, None, None, 0.01) 
    if combination_type == 'comb1':
        fit_parameters['C10mu'       ] = FitParameter('C10mu'       ,0.0, None, None, 0.01)
        fit_parameters['C9pmu'       ] = FitParameter('C9pmu'       ,0.0, None, None, 0.01) 
        fit_parameters['C10pmu'      ] = FitParameter('C10pmu'      ,0.0, None, None, 0.01)
        fit_parameters['delta_CS_CSp'] = FitParameter('delta_CS_CSp',0.0, None, None, 0.01)
        fit_parameters['deltaC9'     ] = FitParameter('deltaC9'     ,0.0, None, None, 0.01)
        fit_parameters['deltaC10'    ] = FitParameter('deltaC10'    ,0.0, None, None, 0.01)
    elif combination_type == 'comb2':
        fit_parameters['C10mu'       ] = FitParameter('C10mu'       ,0.0, None, None, 0.01)
        fit_parameters['C9pmu'       ] = FitParameter('C9pmu'       ,0.0, None, None, 0.01) 
        fit_parameters['C10pmu'      ] = FitParameter('C10pmu'      ,0.0, None, None, 0.01)
        fit_parameters['delta_CS_CSp'] = FitParameter('delta_CS_CSp',0.0, None, None, 0.01)
        fit_parameters['deltaC9'     ] = FitParameter('deltaC9'     ,0.0, None, None, 0.01)
        fit_parameters['deltaC9p'    ] = FitParameter('deltaC9p'    ,0.0, None, None, 0.01)
    elif combination_type == 'comb3':
        fit_parameters['C10mu'       ] = FitParameter('C10mu'       ,0.0, None, None, 0.01)
        fit_parameters['C9pmu'       ] = FitParameter('C9pmu'       ,0.0, None, None, 0.01) 
        fit_parameters['C10pmu'      ] = FitParameter('C10pmu'      ,0.0, None, None, 0.01)
        fit_parameters['delta_CS_CSp'] = FitParameter('delta_CS_CSp',0.0, None, None, 0.01)
        fit_parameters['deltaC9'     ] = FitParameter('deltaC9'     ,0.0, None, None, 0.01)
        fit_parameters['deltaC10p'   ] = FitParameter('deltaC10p'   ,0.0, None, None, 0.01)
    elif combination_type == 'comb4':
        fit_parameters['C10mu'       ] = FitParameter('C10mu'       ,0.0, None, None, 0.01)
        fit_parameters['C9pmu'       ] = FitParameter('C9pmu'       ,0.0, None, None, 0.01) 
        fit_parameters['C10pmu'      ] = FitParameter('C10pmu'      ,0.0, None, None, 0.01)
        fit_parameters['delta_CS_CSp'] = FitParameter('delta_CS_CSp',0.0, None, None, 0.01)
        fit_parameters['deltaC10'    ] = FitParameter('deltaC10'    ,0.0, None, None, 0.01)
        fit_parameters['deltaC10p'   ] = FitParameter('deltaC10p'   ,0.0, None, None, 0.01)
    elif combination_type == 'comb5':
        fit_parameters['C10mu'       ] = FitParameter('C10mu'       ,0.0, None, None, 0.01)
        fit_parameters['C9pmu'       ] = FitParameter('C9pmu'       ,0.0, None, None, 0.01) 
        fit_parameters['C10pmu'      ] = FitParameter('C10pmu'      ,0.0, None, None, 0.01)
        fit_parameters['delta_CS_CSp'] = FitParameter('delta_CS_CSp',0.0, None, None, 0.01)
        fit_parameters['deltaC9p'    ] = FitParameter('deltaC9p'    ,0.0, None, None, 0.01)
        fit_parameters['deltaC10p'   ] = FitParameter('deltaC10p'   ,0.0, None, None, 0.01)
    elif combination_type == 'comb6':
        fit_parameters['C10mu'       ] = FitParameter('C10mu'       ,0.0, None, None, 0.01)
        fit_parameters['C9pmu'       ] = FitParameter('C9pmu'       ,0.0, None, None, 0.01) 
        fit_parameters['C10pmu'      ] = FitParameter('C10pmu'      ,0.0, None, None, 0.01)
        fit_parameters['delta_CS_CSp'] = FitParameter('delta_CS_CSp',0.0, None, None, 0.01)
        fit_parameters['deltaC9p'    ] = FitParameter('deltaC9p'    ,0.0, None, None, 0.01)
        fit_parameters['deltaC10'    ] = FitParameter('deltaC10'    ,0.0, None, None, 0.01)

    #set numpy index
    np.random.seed(toy_index)

    print('WARNING: The fit parameters are', list(fit_parameters.keys()))
    print('WARNING: The constrained parameters are', list(constrained_fit_params.keys()))

    results = main()

    end = time.time()
    print('Time taken in min', (end - start)/60., 'after fitting is complete')
