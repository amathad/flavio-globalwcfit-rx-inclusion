#!/bin/python

import os
import numpy as np

def make_slurm_jobs(sds, suffix, RpK_val):
    njobs   = len(sds)
    command = ''
    indx = 0
    combname = suffix.split('_')[0]
    rxtype   = suffix.split('_')[1]
    for seed in sorted(sds):
        command  += "python fit_"+RpK_val+".py "+str(seed)+" "+combname+" "+rxtype+" rl_value "+str(rl_values[seed])+"\n" #change the RKpi and RKpipi values to 0.86 instead of 1
        #print(command)
        if ((indx%ncluster == 0) and (indx != 0) and indx != njobs) or (indx == njobs-1):
            uniquename = str(indx)+'_'+suffix+'_'+RpK_val
            logname    = fitdir+'log/log_'+uniquename+'.log'
            with open('slurm_scripts/'+uniquename+'.sh', 'w') as file1:
                file1.write('#!/bin/bash\n')
                file1.write('#SBATCH --job-name='+uniquename+'\n')
                file1.write('#SBATCH --output='+logname+'\n')
                file1.write('#SBATCH --partition='+partition+'\n')
                file1.write('#SBATCH --nodes='+numnodes+'\n')
                file1.write('#SBATCH --ntasks='+numtasks+'\n')
                file1.write('#SBATCH --cpus-per-task='+numcpus+'\n')
                #file1.write('#SBATCH --exclude=farm-wn[91-92]\n')
                file1.write('#SBATCH --mem-per-cpu=350M\n')
                file1.write('\n')
                file1.write('cd '+fitdir+'\n')
                file1.write('source ~/farm.sh\n')
                file1.write('conda activate flavio\n')
                file1.write('\n')
                file1.write(command+'\n')
                file1.write('\n')
                file1.write('conda deactivate\n')
                file1.write('conda deactivate\n')
        
            #print(command)
            command = ''
    
        indx += 1

fitdir    = '/home/hep/amathad/Packages/flavio-globalwcfit-rx-inclusion/'
partition = 'express'
numnodes  = '1'
numtasks  = '1'
numcpus   = '1'
ncluster  =  1
npoints   = 40
seeds     = list(range(npoints))
rl_values = np.linspace(-2., 2., npoints)


make_slurm_jobs(seeds , 'comb1_True', '0pt5')
make_slurm_jobs(seeds , 'comb2_True', '0pt5')
make_slurm_jobs(seeds , 'comb3_True', '0pt5')
make_slurm_jobs(seeds , 'comb4_True', '0pt5')
make_slurm_jobs(seeds , 'comb5_True', '0pt5')
make_slurm_jobs(seeds , 'comb6_True', '0pt5')

make_slurm_jobs(seeds , 'comb1_True', '1pt14')
make_slurm_jobs(seeds , 'comb2_True', '1pt14')
make_slurm_jobs(seeds , 'comb3_True', '1pt14')
make_slurm_jobs(seeds , 'comb4_True', '1pt14')
make_slurm_jobs(seeds , 'comb5_True', '1pt14')
make_slurm_jobs(seeds , 'comb6_True', '1pt14')

make_slurm_jobs(seeds , 'comb1_True', '1pt5')
make_slurm_jobs(seeds , 'comb2_True', '1pt5')
make_slurm_jobs(seeds , 'comb3_True', '1pt5')
make_slurm_jobs(seeds , 'comb4_True', '1pt5')
make_slurm_jobs(seeds , 'comb5_True', '1pt5')
make_slurm_jobs(seeds , 'comb6_True', '1pt5')
