#!/bin/python

import os
import pickle

fitdir    = '/home/hep/amathad/Packages/flavio-globalwcfit-rx-inclusion/'
partition = 'long'
numnodes  = '1'
numtasks  = '1'
numcpus   = '1'
suffixes  = []
#suffixes   += ['LFU_RpKLHCb']
#suffixes   += ['RpKLHCb']
#suffixes   += ['RX']
#suffixes   += ['RXWideRange']
#suffixes   += ['RXFixAllr']
#suffixes   += ['RXReducedUncertaintyr7pK']
#suffixes   += ['RXFixr7']
#suffixes   += ['LFU_RpKLHCbReducedUncertainty']
#suffixes   += ['LFU_RpK']
#suffixes   += ['LFU_RpKReducedUncertainty']
#suffixes   += ['LFU_RpKLHCbFixSmallr']
suffixes   += ['ExtendedClean']
suffixes   += ['RestrictedClean']

njobs_min = 3000
njobs_max = 5000
ncluster  = 5
#njobs_max = 5000
#ncluster  = 10
seeds     = list(range(njobs_min, njobs_max)) 

#print('Using failed toys with rxFalse')
#seeds = pickle.load( open( 'failed_rxFalse_toy_LFU_RpKLHCb.p', "rb" ) )
#print(seeds)
#njobs_max = len(seeds)
#ncluster  = 1

for suffix in suffixes:
    cond = suffix != 'RpKLHCb' and suffix != 'LFU_RpKLHCbReducedUncertainty' and suffix != 'LFU_RpK' and suffix != 'LFU_RpKReducedUncertainty' and suffix != 'LFU_RpKLHCbFixSmallr' and suffix != 'RX'
    cond = cond and suffix != 'RXReducedUncertaintyr7pK' and suffix != 'RXFixr7' and suffix != 'RXFixAllr'
    cond = cond and suffix != 'RXWideRange'
    cond = cond and suffix != 'ExtendedClean'
    cond = cond and suffix != 'RestrictedClean'
    if cond:
        ijob = 0
        command   = ''
        for seed in seeds:
            command  += "python fitting_script_"+suffix+".py "+str(seed)+" combSM False toy\n"
            command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb1  False toy\n"
            command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb2  False toy\n"
            command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb3  False toy\n"
            command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb4  False toy\n"
            command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb5  False toy\n"
            command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb6  False toy\n"
            #print(command)
        
            if ((ijob%ncluster == 0) and (ijob != 0) and ijob != njobs_max) or (ijob == njobs_max-1):
                uniquename = str(ijob)
                logname    = fitdir+'log_'+suffix+'/log_'+uniquename+'.log'
                with open('slurm_scripts_toys/'+suffix+'/'+uniquename+'.sh', 'w') as file1:
                    file1.write('#!/bin/bash\n')
                    file1.write('#SBATCH --job-name='+uniquename+'\n')
                    file1.write('#SBATCH --output='+logname+'\n')
                    file1.write('#SBATCH --partition='+partition+'\n')
                    file1.write('#SBATCH --nodes='+numnodes+'\n')
                    file1.write('#SBATCH --ntasks='+numtasks+'\n')
                    file1.write('#SBATCH --cpus-per-task='+numcpus+'\n')
                    #file1.write('#SBATCH --exclude=farm-wn[91-92]\n')
                    #file1.write('#SBATCH --mem-per-cpu=700M\n') #Optimparallel
                    file1.write('#SBATCH --mem-per-cpu=350M\n')

                    file1.write('\n')
                    file1.write('cd '+fitdir+'\n')
                    file1.write('source ~/farm.sh\n')
                    file1.write('conda activate flavio\n')
                    file1.write('\n')
                    file1.write(command+'\n')
                    file1.write('\n')
                    file1.write('conda deactivate\n')
                    file1.write('conda deactivate\n')
            
                #print(command)
                command = ''
        
            ijob += 1
    
    #print('Using failed toys with rxTrue')
    #seeds = pickle.load( open( 'failed_rxTrue_toy_LFU_RpKLHCb.p', "rb" ) ) 
    #print(seeds)
    #njobs_max = len(seeds)
    
    command   = ''
    ijob = 0
    for seed in seeds:
        command  += "python fitting_script_"+suffix+".py "+str(seed)+" combSM True  toy\n"
        command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb1  True  toy\n"
        command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb2  True  toy\n"
        command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb3  True  toy\n"
        command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb4  True  toy\n"
        command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb5  True  toy\n"
        command  += "python fitting_script_"+suffix+".py "+str(seed)+" comb6  True  toy\n"
        #print(command)
        if ((ijob%ncluster == 0) and (ijob != 0) and ijob != njobs_max) or (ijob == njobs_max-1):
            uniquename = str(seed)+'_rx'
            #uniquename = str(ijob)+'_rx'
            logname    = fitdir+'log_'+suffix+'/log_'+uniquename+'_SM.log'
            with open('slurm_scripts_toys/'+suffix+'/'+uniquename+'.sh', 'w') as file1:
                file1.write('#!/bin/bash\n')
                file1.write('#SBATCH --job-name='+uniquename+'\n')
                file1.write('#SBATCH --output='+logname+'\n')
                file1.write('#SBATCH --partition='+partition+'\n')
                file1.write('#SBATCH --nodes='+numnodes+'\n')
                file1.write('#SBATCH --ntasks='+numtasks+'\n')
                file1.write('#SBATCH --cpus-per-task='+numcpus+'\n')
                #file1.write('#SBATCH --exclude=farm-wn[91-92]\n')
                #file1.write('#SBATCH --mem-per-cpu=350M\n') #This was fine with MIGRAD
                file1.write('#SBATCH --mem-per-cpu=350M\n')
                file1.write('\n')
                file1.write('cd '+fitdir+'\n')
                file1.write('source ~/farm.sh\n')
                file1.write('conda activate flavio\n')
                file1.write('\n')
                file1.write(command+'\n')
                file1.write('\n')
                file1.write('conda deactivate\n')
                file1.write('conda deactivate\n')
        
            #print(command)
            command = ''
    
        ijob += 1
