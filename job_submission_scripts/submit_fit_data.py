#!/bin/python

import os

def make_slurm_jobs(sds, suffix, fittype):
    njobs   = len(sds)
    command = ''
    indx = 0
    combname = suffix.split('_')[0]
    rxtype   = suffix.split('_')[1]
    for seed in sorted(sds):
        command  += "python fitting_script_"+fittype+".py "+str(seed)+" "+combname+" "+rxtype+" data\n" #change the RKpi and RKpipi values to 0.86 instead of 1
        #print(command)
        if ((indx%ncluster == 0) and (indx != 0) and indx != njobs) or (indx == njobs-1):
            uniquename = str(indx)+'_'+suffix
            logname    = fitdir+'log_'+fittype+'/log_data_'+uniquename+'.log'
            with open('slurm_scripts_data/'+fittype+'/'+uniquename+'.sh', 'w') as file1:
                file1.write('#!/bin/bash\n')
                file1.write('#SBATCH --job-name='+uniquename+'\n')
                file1.write('#SBATCH --output='+logname+'\n')
                file1.write('#SBATCH --partition='+partition+'\n')
                file1.write('#SBATCH --nodes='+numnodes+'\n')
                file1.write('#SBATCH --ntasks='+numtasks+'\n')
                file1.write('#SBATCH --cpus-per-task='+numcpus+'\n')
                #file1.write('#SBATCH --exclude=farm-wn[91-92]\n')
                file1.write('#SBATCH --mem-per-cpu=350M\n')

                file1.write('\n')
                file1.write('cd '+fitdir+'\n')
                file1.write('source ~/farm.sh\n')
                file1.write('conda activate flavio\n')
                file1.write('\n')
                file1.write(command+'\n')
                file1.write('\n')
                file1.write('conda deactivate\n')
                file1.write('conda deactivate\n')
        
            #print(command)
            command = ''
    
        indx += 1

fitdir    = '/home/hep/amathad/Packages/flavio-globalwcfit-rx-inclusion/'
partition = 'standard'
numnodes  = '1'
numtasks  = '1'
numcpus   = '1'
ncluster  =  1
seeds     = [0]

suffixes = []
#suffixes   += ['LFU_RpKLHCb']
#suffixes   += ['RpKLHCb']
#suffixes   += ['RX']
#suffixes   += ['RXWideRange']
#suffixes   += ['RXFixAllr']

#suffixes   += ['LFU_RpKLHCbReducedUncertainty']
#suffixes   += ['LFU_RpK']
#suffixes   += ['LFU_RpKReducedUncertainty']
#suffixes   += ['LFU_RpKLHCbFixSmallr']
#suffixes   += ['RXReducedUncertaintyr7pK']
#suffixes   += ['RXFixr7']
suffixes   += ['ExtendedClean']
suffixes   += ['RestrictedClean']

for suffix2 in suffixes:
    cond = suffix2 != 'RpKLHCb' and suffix2 != 'LFU_RpKLHCbReducedUncertainty' and suffix2 != 'LFU_RpK' and suffix2 != 'LFU_RpKReducedUncertainty' and suffix2 != 'LFU_RpKLHCbFixSmallr'
    cond = cond and suffix2 != 'RXReducedUncertaintyr7pK' and suffix2 != 'RXFixr7' and suffix2 != 'RXFixAllr'
    cond = cond and suffix2 != 'RXWideRange'
    cond = cond and suffix2 != 'ExtendedClean'
    cond = cond and suffix2 != 'RestrictedClean'
    if cond:
        make_slurm_jobs(seeds , 'combSM_False', suffix2)
        make_slurm_jobs(seeds ,  'comb1_False', suffix2)
        make_slurm_jobs(seeds ,  'comb2_False', suffix2)
        make_slurm_jobs(seeds ,  'comb3_False', suffix2)
        make_slurm_jobs(seeds ,  'comb4_False', suffix2)
        make_slurm_jobs(seeds ,  'comb5_False', suffix2)
        make_slurm_jobs(seeds ,  'comb6_False', suffix2)
    
    make_slurm_jobs(seeds , 'combSM_True', suffix2)
    make_slurm_jobs(seeds ,  'comb1_True', suffix2)
    make_slurm_jobs(seeds ,  'comb2_True', suffix2)
    make_slurm_jobs(seeds ,  'comb3_True', suffix2)
    make_slurm_jobs(seeds ,  'comb4_True', suffix2)
    make_slurm_jobs(seeds ,  'comb5_True', suffix2)
    make_slurm_jobs(seeds ,  'comb6_True', suffix2)
