#!/bin/python

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def make_df(fname, fitres_dict):
    with open(fname) as f:
        for line in f:
            entry = line.split()
            entry_name = entry[0]
            if entry_name in int_names: 
                entry_val = int(entry[1])
                if entry_name == 'is_valid' and entry_val != 1:
                    #pass
                    print('FILE CONTAINS INVALID MINIMA {0}'.format(fname))
                    exit(1)

                if entry_name == 'has_parameters_at_limit' and entry_val == 1:
                    #print('FILE CONTAINS PARAMS AT LIMIT {0}'.format(fname))
                    pass

                if entry_name == 'has_made_posdef_covar' and entry_val == 1:
                    pass
                    #print('FILE CONTAINS NONPOS DEF {0}'.format(fname))
            else:
                entry_val = float(entry[1])
    
            if len(entry) == 3:
                entry_err = entry[2]
                if entry_err == 'nan':
                    #print('Param {0} contains error {1} +/- {2}'.format(entry_name, entry_val, entry_err))
                    print('FILE CONTAINS NAN {0}'.format(fname))
                else:
                    entry_err = float(entry_err)
    
                if entry_name+'_err' in fitres_dict:
                    fitres_dict[entry_name+'_err']+= [entry_err]
                else:
                    fitres_dict[entry_name+'_err'] = [entry_err]
    
            if entry_name in fitres_dict:
                fitres_dict[entry_name]+= [entry_val]
            else:
                fitres_dict[entry_name] = [entry_val]

def get_dataframe(comb_name, RX_value):
    fitres = {}
    fitres['indx']  = []
    fitres['rL_pK'] = []
    for seed in range(npoints):
        fname = './fitresults_'+RX_value+'/result_'+str(seed)+'_'+comb_name+'_rxTrue_'+data_type+'.txt'
        if os.path.isfile(fname): 
            #print(fname)
            fitres['indx']  += [seed]
            fitres['rL_pK'] += [rl_values[seed]]
            make_df(fname, fitres)
        else:
            pass
            #print('FILE DOES NOT EXIST {0}'.format(fname))
    
    fitres = pd.DataFrame(fitres)
    #print(fitres)
    return fitres['loglh'].to_numpy(), fitres['rL_pK'].to_numpy()

int_names = ['is_valid', 'has_parameters_at_limit', 'has_accurate_covar', 'has_posdef_covar', 'has_made_posdef_covar']
combs     = ['comb1', 'comb2', 'comb3', 'comb4', 'comb5', 'comb6']
RX_values = ['0pt5', '0pt86', '1pt14', '1pt5']
data_type = 'rl_value'
npoints   = 40
rl_values = np.linspace(-2., 2., npoints)

rx_dict = {}
rx_dict['0pt5']  = '$R_{pK}=0.5$ '
rx_dict['0pt86'] = '$R_{pK}=0.86$'
rx_dict['1pt14'] = '$R_{pK}=1.14$'
rx_dict['1pt5']  = '$R_{pK}=1.5$ '

for cb in combs:
        print(cb)
        fig = plt.figure()
        ax  = fig.add_subplot(1, 1, 1)
        for RX_value in RX_values:
            lg, rl =  get_dataframe(cb, RX_value)
            ax.scatter(rl, lg, marker='o', label = rx_dict[RX_value])
            #print('{0}: {1:1.2f}'.format(RX_value, np.max(lg) - np.min(lg)))
            print('{0:1.2f}'.format(np.max(lg) - np.min(lg)))

        ax.legend(loc='best')
        ax.set_xlabel('rL_pK', fontsize=20)
        ax.set_ylabel(r'$-2 \times log(L)$', fontsize=20)
        ax.set_title(cb+'($R_{pK}$ rel. unc. 17.3%)', fontsize=20)
        fig.tight_layout()
        fig.savefig('./plots/rl_sensitivity_'+cb+'_'+RX_value+'.png')
