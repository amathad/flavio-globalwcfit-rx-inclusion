# Flavio GlobalWCFit RX Inclusion

Code for the project used to investigate inclusion of R(X) = R(pK), R(Kpi) and R(Kpipi) results in the Global WC fits. 

This project depends on [Flavio](https://flav-io.github.io/) and particularly on the [forked branch](https://github.com/abhijitm08/flavio.git) where the full likelihood of `B0->K*mumu` angular observables and their correlation is included. 


```bash
git clone https://github.com/abhijitm08/flavio.git
cd flavio
python -m pip install -e .[plotting,testing] --user
```
