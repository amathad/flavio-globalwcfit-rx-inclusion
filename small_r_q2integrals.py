#flavio
import flavio
from wilson import Wilson
from flavio.physics.eft import WilsonCoefficients
from flavio.physics.bdecays.wilsoncoefficients import wctot_dict

#mathplotlib
import matplotlib.pyplot as plt
import wilson
plt.rc('grid', linestyle="--", color='black', alpha = 0.9)

#other libraries
import numpy as np
import scipy
import pprint
import pickle

def get_wilson_sm(scale = 4.8, sector = 'bsmumu'):
    # Get SM wilson coefficients
    wc   = flavio.WilsonCoefficients() 
    par  = flavio.default_parameters.get_central_all()
    wc_sm= wctot_dict(wc_obj=wc, sector=sector, scale=scale, par=par, nf_out=5)
    return wc_sm

def get_wilson_np(wc_sm, ll_den =None, ll_num = None, r_param_name = None, scale = 4.8):
    wc_np = {}
    if ll_den is not None and ll_num is None: #C9=1 and rest are zero
        if r_param_name is not None:
            raise Exception('Passed ll',ll_den,', but r_param_name is not None. Please check!')

        wc_np['C9_bs'+ll_den   ] = -1.0 * wc_sm['C9_bsmumu' ] + 1.0
        wc_np['C10_bs'+ll_den  ] = -1.0 * wc_sm['C10_bsmumu']
        wc_np['C9p_bs'+ll_den  ] =  0.0
        wc_np['C10p_bs'+ll_den ] =  0.0
        wc_np['C7_bs'      ]     = -1.0 * wc_sm['C7eff_bs'  ]
        wc_np['C7p_bs'     ]     = -1.0 * wc_sm['C7effp_bs' ]
    elif ll_den is None and ll_num is not None: 
        if r_param_name is None:
            raise Exception('Passed ll ',ll_num,' but r_param_name is None. Please check!')

        if r_param_name == 'rL': #C9mu=C9pmu=1 and rest zero
            wc_np['C9_bs'+ll_num  ] = -1.0 * wc_sm['C9_bsmumu' ] + 1.0
            wc_np['C10_bs'+ll_num ] = -1.0 * wc_sm['C10_bsmumu']
            wc_np['C9p_bs'+ll_num ] =   1.0
            wc_np['C10p_bs'+ll_num] =   0.0
            wc_np['C7_bs'     ]     = -1.0 * wc_sm['C7eff_bs'  ]
            wc_np['C7p_bs'    ]     = -1.0 * wc_sm['C7effp_bs' ]
        elif r_param_name == 'r7': #C7=1 and rest zero
            wc_np['C9_bs'+ll_num  ] = -1.0 * wc_sm['C9_bsmumu' ]
            wc_np['C10_bs'+ll_num ] = -1.0 * wc_sm['C10_bsmumu']
            wc_np['C9p_bs'+ll_num ] =   0.0
            wc_np['C10p_bs'+ll_num] =   0.0
            wc_np['C7_bs'     ]     = -1.0 * wc_sm['C7eff_bs'  ] + 1.0
            wc_np['C7p_bs'    ]     = -1.0 * wc_sm['C7effp_bs' ]
        elif r_param_name == 'r1': #C7=C9=1 and rest zero
            wc_np['C9_bs'+ll_num  ] = -1.0 * wc_sm['C9_bsmumu' ] + 1.0
            wc_np['C10_bs'+ll_num ] = -1.0 * wc_sm['C10_bsmumu']
            wc_np['C9p_bs'+ll_num ] =   0.0
            wc_np['C10p_bs'+ll_num] =   0.0
            wc_np['C7_bs'     ]     = -1.0 * wc_sm['C7eff_bs'  ] + 1.0
            wc_np['C7p_bs'    ]     = -1.0 * wc_sm['C7effp_bs' ]
        elif r_param_name == 'r2': #C7=C9p=1 and rest zero
            wc_np['C9_bs'+ll_num  ] = -1.0 * wc_sm['C9_bsmumu' ]
            wc_np['C10_bs'+ll_num ] = -1.0 * wc_sm['C10_bsmumu']
            wc_np['C9p_bs'+ll_num ] =   1.0
            wc_np['C10p_bs'+ll_num] =   0.0
            wc_np['C7_bs'     ]     = -1.0 * wc_sm['C7eff_bs'  ] + 1.0
            wc_np['C7p_bs'    ]     = -1.0 * wc_sm['C7effp_bs' ]
        else:
            raise Exception('The r_param_name not recognised', r_param_name)
    else:
        raise Exception('The ll_den and ll_num are both None')

    wilson_np = Wilson(wc_np, scale=scale, eft='WET', basis='flavio')
    return wilson_np

def convert_rrel_rval(r_param_name, r_rKst_vals, r7_vals = None):
    '''
    When r_param_name = rL the ratio should correspond to (2 + rL)
    When r_param_name = r7 the ratio should correspond to (r7)
    When r_param_name = r1 the ratio should correspond to (1 + r7 + r1)
    When r_param_name = r2 the ratio should correspond to (1 + r7 + r2)
    Return:
        Convert the ratio values related to r(q2) params to actual r(q2) values
    '''
    if r_param_name == 'r1' or r_param_name == 'r2':
        if r7_vals is None:
            raise Exception('The values for r7_vals not passed for r1 and r2 determination!')

    rval = None
    if r_param_name == 'rL':
        rval = r_rKst_vals - 2.
    elif r_param_name == 'r7':
        rval = r_rKst_vals
    elif r_param_name == 'r1' or r_param_name == 'r2':
        rval = r_rKst_vals - 1. - r7_vals
    else:
        raise Exception('Not recognised r param name!', r_param_name)
    
    return rval

def get_rKst_flavio_new(q2v, wc_d, wc_n, ll, get_denominator_only = False):
    '''
    dG/q2 for:
        m = 2 * f(q2)
        mu when rL = f(q2) * (4 + 2 * rL)
        mu when r7 = f(q2) * (2 * r7)
        mu when r1 = 2 * f(q2) * (1 + r7 + r1)
        mu when r2 = 2 * f(q2) * (1 + r7 + r2)
    Return:
        Ratio of dG/dq^2 for mu and e for given wc_e, wc_mu and q2 values
    '''
    #get dG/dq2 for electrons and muons for different q2 values
    fval_d = np.array([kstll_obs_vals(q2i, wc_d, ll) for q2i in q2v]) 
    if get_denominator_only:
        return fval_d
    else:
        fval_n = np.array([kstll_obs_vals(q2i, wc_n, ll) for q2i in q2v]) 
        r_vals = fval_n/fval_d 
        return r_vals

def kstll_obs_vals(q2, wc_np, ll):
    #make q2 functions for dBR/dq2 for e 
    if ll == 'ee' or ll == 'mumu':
        return flavio.np_prediction('dBR/dq2(B0->K*'+ll+')', wc_np , q2) 
        #return flavio.np_prediction('dBR/dq2(B+->K'+ll+')', wc_np , q2) 
        #return flavio.np_prediction('dBR/dq2(B0->K'+ll+')', wc_np , q2) 
    else:
        raise Exception('The ll param not recognised', ll)

def get_rKst_flavio_intg(q2_min, q2_max, wc_d, wc_n, inverse = False):
    '''
    <dG/q2> for:
        e = 2 * <f(q2)>
        mu when rL = <f(q2)> * (4 + 2 * <rL>)
        mu when r7 = <f(q2)> * (2 * <r7>)
        mu when r1 = 2 * <f(q2)> * (1 + <r7> + <r1>)
        mu when r2 = 2 * <f(q2)> * (1 + <r7> + <r2>)
    Return:
        Ratio of <dG/dq^2> for mu and e for given wc_e, wc_mu and q2 values
    '''
    #get <dG/dq2> for electrons and muons for different q2 values
    if inverse:
        fval_n = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_n , 'ee' )) 
        fval_d = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_d, 'mumu')) 
    else:
        fval_n = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_n, 'mumu')) 
        fval_d = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_d , 'ee' )) 

    r_vals  = fval_n/fval_d 
    return r_vals

def get_rKst_flavio_intg_new(q2_min, q2_max, wc_d, wc_n, ll):
    '''
    <dG/q2> for:
        mu = 2 * <f(q2)>
        mu when rL = <f(q2)> * (4 + 2 * <rL>)
        mu when r7 = <f(q2)> * (2 * <r7>)
        mu when r1 = 2 * <f(q2)> * (1 + <r7> + <r1>)
        mu when r2 = 2 * <f(q2)> * (1 + <r7> + <r2>)
    Return:
        Ratio of <dG/dq^2> for mu and e for given wc_e, wc_mu and q2 values
    '''
    #get <dG/dq2> for electrons and muons for different q2 values
    fval_n = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_n, ll))  #Note that integrals are in units of GeV^2
    fval_d = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_d, ll)) 
    r_vals = fval_n/fval_d 
    return r_vals

def kstll_obs_vals_intg(q2_min, q2_max, wc_np, ll):
    intgl_smpl = np.random.uniform(q2_min, q2_max, size=100)
    dBR = lambda q2: flavio.np_prediction('dBR/dq2(B0->K*'+ll+')', wc_np , q2)
    #print('intgl', intgl_smpl)
    #make q2 functions for dBR/dq2 for e 
    if ll == 'ee' or ll == 'mumu':
        return scipy.integrate.quad(dBR, q2_min, q2_max, epsabs=1.49e-10, epsrel=1.49e-10, limit=100)[0]
        #return scipy.integrate.quad(dBR, q2_min, q2_max)[0]
        #return flavio.np_prediction('<dBR/dq2>(B0->K*'+ll+')', wc_np , q2min = q2_min, q2max = q2_max) 
        #dGdq2_arr = np.array([flavio.np_prediction('dBR/dq2(B0->K*'+ll+')', wc_np , q2) for q2 in intgl_smpl])
        #print('q2_arr', q2_arr)
        #print('mine', (q2_max - q2_min) * np.mean(dGdq2_arr))
        #print('flavio', (q2_max - q2_min) * flavio.np_prediction('<dBR/dq2>(B0->K*'+ll+')', wc_np , q2min = q2_min, q2max = q2_max))
        #exit(1)
        #return flavio.np_prediction('dBR/dq2(B0->K*'+ll+')', wc_np , q2) 
        #return flavio.np_prediction('<dBR/dq2>(B+->K'+ll+')', wc_np , q2min = q2_min, q2max = q2_max) 
        ##return flavio.np_prediction('<dBR/dq2>(B0->K'+ll+')', wc_np , q2min = q2_min, q2max = q2_max) 
        #return (q2_max - q2_min) * np.mean(dGdq2_arr) 
    else:
        raise Exception('The ll param not recognised', ll)

def plot_r_param(r_param_name, ylabel, xvals, yvals, labels, colors, fname_suffix, markers=None, y_scale = 'linear', bin_width = None):
    #make labels
    xlabel = '$q_{min}^{2} [GeV^{2}]$'
    if len(labels) != len(yvals):
        raise Exception('Length of labels do not match y values. Check!')

    #plot the result
    fig = plt.figure()
    ax  = fig.add_subplot(1, 1, 1)
    for i in range(len(yvals)): 
        if markers is not None:
            if bin_width is not None:
                ax.errorbar(xvals, yvals[i], xerr = 0.5 * bin_width, fmt = 'o', color=colors[i], label = labels[i])
            else:
                #ax.scatter(xvals, yvals[i], marker=markers[i], color=colors[i], label = labels[i])
                #ax.plot(xvals, yvals[i], marker=markers[i], color=colors[i], label = labels[i])
                #ax.plot(xvals, yvals[i], linestyle='-', color=colors[i], label = labels[i])
                ax.plot(xvals, yvals[i], linestyle='-', color=colors[i], label = labels[i])
        else:
            if bin_width is not None:
                ax.errorbar(xvals, yvals[i], xerr = 0.5 * bin_width, fmt = 'o', color=colors[i], label = labels[i])
            else:
                #ax.scatter(xvals, yvals[i], marker='o', color=colors[i], label = labels[i])
                #ax.plot(xvals, yvals[i], marker='o', color=colors[i], label = labels[i])
                ax.plot(xvals, yvals[i], linestyle='-', color=colors[i], label = labels[i])

    if len(yvals) > 1: ax.legend(loc='best')
    ax.set_xlabel(xlabel, fontsize=30)
    ax.set_yscale(y_scale)
    ax.set_ylabel(ylabel, fontsize=30)
    if y_scale == 'log': ax.set_ylabel(ylabel + '(log)', fontsize=20)
    #ax.axvline(0.1, color='red')
    #ax.axvline(1.1, color='green')
    #ax.axvspan(0.98, 1.10, alpha=0.5, color='red')
    #ax.axvspan(8.00, 11.0, alpha=0.5, color='red')
    #ax.axvspan(12.5, 15.0, alpha=0.5, color='red')
    fig.tight_layout()
    fig.savefig('./plots/'+r_param_name+'_'+fname_suffix+'.pdf')

def plot_r_param_new(xvals, yvals):
    #make labels
    xlabel   = '$q_{min}^{2} [GeV^{2}]$'
    ylabels  = []
    ylabels += [r'$\left<\eta_{K^{*}}^{0}\right>$']
    ylabels += [r'$\left<\eta_{K^{*}}^{77}\right>$']
    ylabels += [r'$\left<\eta_{K^{*}}^{79}\right>$']
    ylabels += [r'$\left<\eta_{K^{*}}^{79^\prime}\right>$']
    #plot the result
    rows, cols = 2, 3
    fig, ax = plt.subplots(2, 2, sharex=True, figsize=(8,6))
    ax[0,0].plot(xvals, yvals[0], linestyle='-', linewidth=3, color='b')
    ax[1,0].plot(xvals, yvals[1], linestyle='-', linewidth=3, color='b')
    ax[0,1].plot(xvals, yvals[2], linestyle='-', linewidth=3, color='b')
    ax[1,1].plot(xvals, yvals[3], linestyle='-', linewidth=3, color='b')
    #ax[0,0].set_xlabel(xlabel, fontsize=20)
    #ax[1,0].set_xlabel(xlabel, fontsize=20)
    #ax[0,1].set_xlabel(xlabel, fontsize=20)
    #ax[1,1].set_xlabel(xlabel, fontsize=20)
    ax[0,0].set_ylabel(ylabels[0], fontsize=20)
    ax[1,0].set_ylabel(ylabels[1], fontsize=20)
    ax[0,1].set_ylabel(ylabels[2], fontsize=20)
    ax[1,1].set_ylabel(ylabels[3], fontsize=20)
    #plt.xticks(fontsize=16)
    #plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    #plt.xlabel(xlabel)
    #fig.text(0.55, 0.04, xlabel, ha='center', fontsize=20)
    # add a big axis, hide frame
    fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axis
    plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    plt.xlabel(xlabel, fontsize=20)
    fig.tight_layout()
    fig.savefig('./plots/rvals.pdf', bbox_inches='tight')

def plot_r_param_mue_ratio(r_param_name, ylabel, xvals, yvals, fname_suffix):
    #make labels
    xlabel = '$q^{2} [GeV^{2}]$'
    #plot the result
    fig = plt.figure()
    ax  = fig.add_subplot(1, 1, 1)
    ax.scatter(xvals, yvals, marker='o', color='orange')
    ax.set_xlabel(xlabel, fontsize=20)
    ax.set_ylabel(ylabel, fontsize=20)
    #ax.axvspan(0.98, 1.10, alpha=0.5, color='red')
    #ax.axvspan(8.00, 11.0, alpha=0.5, color='red')
    #ax.axvspan(12.5, 15.0, alpha=0.5, color='red')
    fig.tight_layout()
    fig.savefig('./plots/'+r_param_name+'_'+fname_suffix+'.png')

def dGOdq2_gino(wc, par, sector, ll):
    #define a Gamma functions
    Gamma = lambda a: (C9 - a * C10)**2 + (C9p - a * C10p)**2 + rL * (C9 - a * C10) * (C9p - a * C10p) + C7 * (r7 * C7 + r1 * (C9 - a * C10) + r2 * (C9p - a * C10p))
    #get r_i
    rL    = np.array(par['rL'])
    r7    = np.array(par['r7'])
    r1    = np.array(par['r1'])
    r2    = np.array(par['r2'])
    #print(rL, r7, r1, r2)
    #get wc
    C7    = wc['C7eff_'+sector]      
    C9    = wc['C9_'+sector+ll]
    C9p   = wc['C9p_'+sector+ll]
    C10   = wc['C10_'+sector+ll]
    C10p  = wc['C10p_'+sector+ll]
    #print('Gino', sector, ll)
    #print('C7eff_'+sector  , wc['C7eff_'+sector])
    #print('C9_'+sector+ll  , wc['C9_'+sector+ll])
    #print('C9p_'+sector+ll , wc['C9p_'+sector+ll])
    #print('C10_'+sector+ll , wc['C10_'+sector+ll])
    #print('C10p_'+sector+ll, wc['C10p_'+sector+ll])
    #print(Gamma(1.) + Gamma(-1.))
    return Gamma(1.) + Gamma(-1.)

def get_rKst_gino(wilson_np, params, r_mu_dict, r_e_dict, scale = 4.8):
    #get SM + NP WCs
    wcs   = flavio.WilsonCoefficients() 
    wc_np = wilson_np.wc.dict
    wcs.set_initial(wc_np, scale = 4.8)
    wc_tot_mu  = wctot_dict(wc_obj=wcs, sector='bsmumu', scale=scale, par=params, nf_out=5)
    wc_tot_e   = wctot_dict(wc_obj=wcs, sector='bsee'  , scale=scale, par=params, nf_out=5)

    #dG/dq2 muons
    dgdq2_mu = dGOdq2_gino(wc_tot_mu, r_mu_dict, 'bs', 'mumu')
    dgdq2_e  = dGOdq2_gino(wc_tot_e, r_e_dict  , 'bs', 'ee')
    #make RKst
    rKst = dgdq2_mu/dgdq2_e
    return rKst

def get_rKst_gino_intg(wilson_np, params, r_mu_dict, r_e_dict, scale = 4.8):
    #get SM + NP WCs
    wcs   = flavio.WilsonCoefficients() 
    wc_np = wilson_np.wc.dict
    wcs.set_initial(wc_np, scale = 4.8)
    wc_tot_mu  = wctot_dict(wc_obj=wcs, sector='bsmumu', scale=scale, par=params, nf_out=5)
    wc_tot_e   = wctot_dict(wc_obj=wcs, sector='bsee'  , scale=scale, par=params, nf_out=5)

    #dG/dq2 muons
    dgdq2_mu = dGOdq2_gino(wc_tot_mu, r_mu_dict, 'bs', 'mumu')
    dgdq2_e  = dGOdq2_gino(wc_tot_e, r_e_dict  , 'bs', 'ee')
    #make RKst
    rKst = dgdq2_mu/dgdq2_e
    return rKst

def main():
    #define four q2 ranges vetoeing phi, jpsi, psi(2S), these ranges are used in Kstmumu analysis
    mmu       = 105.65e-3
    q2_lowmu  = 4. * mmu**2
    q2_edges  = np.linspace(q2_lowmu, 6.0, 100)
    q2_bw     = q2_edges[1:] - q2_edges[:-1]
    q2_vals   = q2_edges[:-1] + 0.5 * q2_bw
    q2_ranges = []
    #for q2_min, q2_max in zip(q2_edges[:-1], q2_edges[1:]): q2_ranges += [(q2_min, q2_max)]
    for q2_min, q2_max in zip(q2_edges[:-1], q2_edges[1:]): q2_ranges += [(q2_min, 6.0)]
    print(q2_ranges)

    #get SM WC
    wc_sm     = get_wilson_sm()
    #Set the new physics WC accordingly once for electron and muon (C9=1 and rest 0 for electron) to get r_i values
    wc_np_d_e = get_wilson_np(wc_sm, ll_den = 'ee')
    wc_np_d_m = get_wilson_np(wc_sm, ll_den = 'mumu')
    #Set the new physics WC accordingly once for electron and muon (here the settings are r name dependent), again used to obtain r_i values
    wc_np_num_rL_mu= get_wilson_np(wc_sm, ll_num = 'mumu', r_param_name='rL')
    wc_np_num_r7_mu= get_wilson_np(wc_sm, ll_num = 'mumu', r_param_name='r7')
    wc_np_num_r1_mu= get_wilson_np(wc_sm, ll_num = 'mumu', r_param_name='r1')
    wc_np_num_r2_mu= get_wilson_np(wc_sm, ll_num = 'mumu', r_param_name='r2')
    wc_np_num_rL_e = get_wilson_np(wc_sm, ll_num = 'ee', r_param_name='rL')
    wc_np_num_r7_e = get_wilson_np(wc_sm, ll_num = 'ee', r_param_name='r7')
    wc_np_num_r1_e = get_wilson_np(wc_sm, ll_num = 'ee', r_param_name='r1')
    wc_np_num_r2_e = get_wilson_np(wc_sm, ll_num = 'ee', r_param_name='r2')

    #make containers to put things q2 and r_i values in
    rL_vals_mu = []
    r7_vals_mu = []
    r1_vals_mu = []
    r2_vals_mu = []
    rL_vals_e  = []
    r7_vals_e  = []
    r1_vals_e  = []
    r2_vals_e  = []
    for q2r in q2_ranges:
        print(q2r)
        #calculate fl
        q2min, q2max = q2r
        #mu vals
        rL_rKst_mu = get_rKst_flavio_intg_new(q2min, q2max, wc_np_d_m, wc_np_num_rL_mu, 'mumu')
        r7_rKst_mu = get_rKst_flavio_intg_new(q2min, q2max, wc_np_d_m, wc_np_num_r7_mu, 'mumu')
        r1_rKst_mu = get_rKst_flavio_intg_new(q2min, q2max, wc_np_d_m, wc_np_num_r1_mu, 'mumu')
        r2_rKst_mu = get_rKst_flavio_intg_new(q2min, q2max, wc_np_d_m, wc_np_num_r2_mu, 'mumu')
        ## evals
        #rL_rKst_e  = get_rKst_flavio_intg_new(q2min, q2max, wc_np_d_e, wc_np_num_rL_e, 'ee')
        #r7_rKst_e  = get_rKst_flavio_intg_new(q2min, q2max, wc_np_d_e, wc_np_num_r7_e, 'ee')
        #r1_rKst_e  = get_rKst_flavio_intg_new(q2min, q2max, wc_np_d_e, wc_np_num_r1_e, 'ee')
        #r2_rKst_e  = get_rKst_flavio_intg_new(q2min, q2max, wc_np_d_e, wc_np_num_r2_e, 'ee')
        #small r values mu
        rL_val_mu  = convert_rrel_rval('rL', rL_rKst_mu)
        r7_val_mu  = convert_rrel_rval('r7', r7_rKst_mu)
        r1_val_mu  = convert_rrel_rval('r1', r1_rKst_mu, r7_vals = r7_rKst_mu)
        r2_val_mu  = convert_rrel_rval('r2', r2_rKst_mu, r7_vals = r7_rKst_mu)
        ##small r values e
        #rL_val_e  = convert_rrel_rval('rL', rL_rKst_e)
        #r7_val_e  = convert_rrel_rval('r7', r7_rKst_e)
        #r1_val_e  = convert_rrel_rval('r1', r1_rKst_e, r7_vals = r7_rKst_e)
        #r2_val_e  = convert_rrel_rval('r2', r2_rKst_e, r7_vals = r7_rKst_e)
        #put them into containers
        rL_vals_mu += [rL_val_mu]
        r7_vals_mu += [r7_val_mu]
        r1_vals_mu += [r1_val_mu]
        r2_vals_mu += [r2_val_mu]
        #rL_vals_e  += [rL_val_e]
        #r7_vals_e  += [r7_val_e]
        #r1_vals_e  += [r1_val_e]
        #r2_vals_e  += [r2_val_e]

    rL_vals_mu = np.array(rL_vals_mu)
    r7_vals_mu = np.array(r7_vals_mu)
    r1_vals_mu = np.array(r1_vals_mu)
    r2_vals_mu = np.array(r2_vals_mu)
    #rL_vals_e  = np.array(rL_vals_e )
    #r7_vals_e  = np.array(r7_vals_e )
    #r1_vals_e  = np.array(r1_vals_e )
    #r2_vals_e  = np.array(r2_vals_e )
    print(len(rL_vals_mu))
    print(len(q2_vals))
    print(len(q2_bw))
    #plot_r_param('rL_BToKstarll', ylabel=r'$\left<r_{L}\right>(q_{min}^2)$', xvals=q2_vals, yvals=[rL_vals_mu, rL_vals_e], labels=['$\mu$', 'e'], colors=['b', 'g'], fname_suffix='intg')
    #plot_r_param('r7_BToKstarll', ylabel=r'$\left<r_{7}\right>(q_{min}^2)$', xvals=q2_vals, yvals=[r7_vals_mu, r7_vals_e], labels=['$\mu$', 'e'], colors=['b', 'g'], fname_suffix='intg')
    #plot_r_param('r1_BToKstarll', ylabel=r'$\left<r_{1}\right>(q_{min}^2)$', xvals=q2_vals, yvals=[r1_vals_mu, r1_vals_e], labels=['$\mu$', 'e'], colors=['b', 'g'], fname_suffix='intg')
    #plot_r_param('r2_BToKstarll', ylabel=r'$\left<r_{2}\right>(q_{min}^2)$', xvals=q2_vals, yvals=[r2_vals_mu, r2_vals_e], labels=['$\mu$', 'e'], colors=['b', 'g'], fname_suffix='intg')
    #plot_r_param('rL_BToKstarll', ylabel=r'$\left<\eta_{K^{*}}^{0}\right>$', xvals=q2_vals, yvals=[rL_vals_mu], labels=[''], colors=['b'], fname_suffix='intg')
    #plot_r_param('r7_BToKstarll', ylabel=r'$\left<\eta_{K^{*}}^{77}\right>$', xvals=q2_vals, yvals=[r7_vals_mu], labels=[''], colors=['b'], fname_suffix='intg')
    #plot_r_param('r1_BToKstarll', ylabel=r'$\left<\eta_{K^{*}}^{79}\right>$', xvals=q2_vals, yvals=[r1_vals_mu], labels=[''], colors=['b'], fname_suffix='intg')
    #plot_r_param('r2_BToKstarll', ylabel=r'$\left<\eta_{K^{*}}^{79^\prime}\right>$', xvals=q2_vals, yvals=[r2_vals_mu], labels=[''], colors=['b'], fname_suffix='intg')
    yvals    = []
    yvals   += [rL_vals_mu]
    yvals   += [r7_vals_mu]
    yvals   += [r1_vals_mu]
    yvals   += [r2_vals_mu]
    pickle.dump( (q2_vals, yvals), open( "plots/rvals.p", "wb" ) )

def plot_rvals():
    q2_vals, yvals = pickle.load( open( "plots/rvals.p", "rb" ) )
    plot_r_param_new(xvals=q2_vals, yvals=yvals)

if __name__ == '__main__':
    #main()
    plot_rvals()