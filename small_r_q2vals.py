#flavio
import flavio
from wilson import Wilson
from flavio.physics.eft import WilsonCoefficients
from flavio.physics.bdecays.wilsoncoefficients import wctot_dict

#mathplotlib
import matplotlib.pyplot as plt
import wilson
plt.rc('grid', linestyle="--", color='black', alpha = 0.9)

#other libraries
import numpy as np
import scipy
import pprint
import pickle

def get_wilson_sm(scale = 4.8, sector = 'bsmumu'):
    # Get SM wilson coefficients
    wc   = flavio.WilsonCoefficients() 
    par  = flavio.default_parameters.get_central_all()
    wc_sm= wctot_dict(wc_obj=wc, sector=sector, scale=scale, par=par, nf_out=5)
    return wc_sm

def get_wilson_np(wc_sm, ll_den =None, ll_num = None, r_param_name = None, scale = 4.8):
    wc_np = {}
    if ll_den is not None and ll_num is None: #C9=1 and rest are zero
        if r_param_name is not None:
            raise Exception('Passed ll',ll_den,', but r_param_name is not None. Please check!')

        wc_np['C9_bs'+ll_den   ] = -1.0 * wc_sm['C9_bsmumu' ] + 1.0
        wc_np['C10_bs'+ll_den  ] = -1.0 * wc_sm['C10_bsmumu']
        wc_np['C9p_bs'+ll_den  ] =  0.0
        wc_np['C10p_bs'+ll_den ] =  0.0
        wc_np['C7_bs'      ]     = -1.0 * wc_sm['C7eff_bs'  ]
        wc_np['C7p_bs'     ]     = -1.0 * wc_sm['C7effp_bs' ]
    elif ll_den is None and ll_num is not None: 
        if r_param_name is None:
            raise Exception('Passed ll ',ll_num,' but r_param_name is None. Please check!')

        if r_param_name == 'rL': #C9mu=C9pmu=1 and rest zero
            wc_np['C9_bs'+ll_num  ] = -1.0 * wc_sm['C9_bsmumu' ] + 1.0
            wc_np['C10_bs'+ll_num ] = -1.0 * wc_sm['C10_bsmumu']
            wc_np['C9p_bs'+ll_num ] =   1.0
            wc_np['C10p_bs'+ll_num] =   0.0
            wc_np['C7_bs'     ]     = -1.0 * wc_sm['C7eff_bs'  ]
            wc_np['C7p_bs'    ]     = -1.0 * wc_sm['C7effp_bs' ]
        elif r_param_name == 'r7': #C7=1 and rest zero
            wc_np['C9_bs'+ll_num  ] = -1.0 * wc_sm['C9_bsmumu' ]
            wc_np['C10_bs'+ll_num ] = -1.0 * wc_sm['C10_bsmumu']
            wc_np['C9p_bs'+ll_num ] =   0.0
            wc_np['C10p_bs'+ll_num] =   0.0
            wc_np['C7_bs'     ]     = -1.0 * wc_sm['C7eff_bs'  ] + 1.0
            wc_np['C7p_bs'    ]     = -1.0 * wc_sm['C7effp_bs' ]
        elif r_param_name == 'r1': #C7=C9=1 and rest zero
            wc_np['C9_bs'+ll_num  ] = -1.0 * wc_sm['C9_bsmumu' ] + 1.0
            wc_np['C10_bs'+ll_num ] = -1.0 * wc_sm['C10_bsmumu']
            wc_np['C9p_bs'+ll_num ] =   0.0
            wc_np['C10p_bs'+ll_num] =   0.0
            wc_np['C7_bs'     ]     = -1.0 * wc_sm['C7eff_bs'  ] + 1.0
            wc_np['C7p_bs'    ]     = -1.0 * wc_sm['C7effp_bs' ]
        elif r_param_name == 'r2': #C7=C9p=1 and rest zero
            wc_np['C9_bs'+ll_num  ] = -1.0 * wc_sm['C9_bsmumu' ]
            wc_np['C10_bs'+ll_num ] = -1.0 * wc_sm['C10_bsmumu']
            wc_np['C9p_bs'+ll_num ] =   1.0
            wc_np['C10p_bs'+ll_num] =   0.0
            wc_np['C7_bs'     ]     = -1.0 * wc_sm['C7eff_bs'  ] + 1.0
            wc_np['C7p_bs'    ]     = -1.0 * wc_sm['C7effp_bs' ]
        else:
            raise Exception('The r_param_name not recognised', r_param_name)
    else:
        raise Exception('The ll_den and ll_num are both None')

    wilson_np = Wilson(wc_np, scale=scale, eft='WET', basis='flavio')
    return wilson_np

def convert_rrel_rval(r_param_name, r_rKst_vals, r7_vals = None):
    '''
    When r_param_name = rL the ratio should correspond to (2 + rL)
    When r_param_name = r7 the ratio should correspond to (r7)
    When r_param_name = r1 the ratio should correspond to (1 + r7 + r1)
    When r_param_name = r2 the ratio should correspond to (1 + r7 + r2)
    Return:
        Convert the ratio values related to r(q2) params to actual r(q2) values
    '''
    if r_param_name == 'r1' or r_param_name == 'r2':
        if r7_vals is None:
            raise Exception('The values for r7_vals not passed for r1 and r2 determination!')

    rval = None
    if r_param_name == 'rL':
        rval = r_rKst_vals - 2.
    elif r_param_name == 'r7':
        rval = r_rKst_vals
    elif r_param_name == 'r1' or r_param_name == 'r2':
        rval = r_rKst_vals - 1. - r7_vals
    else:
        raise Exception('Not recognised r param name!', r_param_name)
    
    return rval

def get_rKst_flavio_new(q2v, wc_d, wc_n, ll, get_denominator_only = False):
    '''
    dG/q2 for:
        m = 2 * f(q2)
        mu when rL = f(q2) * (4 + 2 * rL)
        mu when r7 = f(q2) * (2 * r7)
        mu when r1 = 2 * f(q2) * (1 + r7 + r1)
        mu when r2 = 2 * f(q2) * (1 + r7 + r2)
    Return:
        Ratio of dG/dq^2 for mu and e for given wc_e, wc_mu and q2 values
    '''
    #get dG/dq2 for electrons and muons for different q2 values
    fval_d = np.array([kstll_obs_vals(q2i, wc_d, ll) for q2i in q2v]) 
    if get_denominator_only:
        return fval_d
    else:
        fval_n = np.array([kstll_obs_vals(q2i, wc_n, ll) for q2i in q2v]) 
        r_vals = fval_n/fval_d 
        return r_vals

def kstll_obs_vals(q2, wc_np, ll):
    #make q2 functions for dBR/dq2 for e 
    if ll == 'ee' or ll == 'mumu':
        return flavio.np_prediction('dBR/dq2(B0->K*'+ll+')', wc_np , q2) 
        #return flavio.np_prediction('dBR/dq2(B+->K'+ll+')', wc_np , q2) 
        #return flavio.np_prediction('dBR/dq2(B0->K'+ll+')', wc_np , q2) 
    else:
        raise Exception('The ll param not recognised', ll)

def get_rKst_flavio_intg(q2_min, q2_max, wc_d, wc_n, inverse = False):
    '''
    <dG/q2> for:
        e = 2 * <f(q2)>
        mu when rL = <f(q2)> * (4 + 2 * <rL>)
        mu when r7 = <f(q2)> * (2 * <r7>)
        mu when r1 = 2 * <f(q2)> * (1 + <r7> + <r1>)
        mu when r2 = 2 * <f(q2)> * (1 + <r7> + <r2>)
    Return:
        Ratio of <dG/dq^2> for mu and e for given wc_e, wc_mu and q2 values
    '''
    #get <dG/dq2> for electrons and muons for different q2 values
    if inverse:
        fval_n = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_n , 'ee' )) 
        fval_d = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_d, 'mumu')) 
    else:
        fval_n = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_n, 'mumu')) 
        fval_d = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_d , 'ee' )) 

    r_vals  = fval_n/fval_d 
    return r_vals

def get_rKst_flavio_intg_new(q2_min, q2_max, wc_d, wc_n, ll):
    '''
    <dG/q2> for:
        mu = 2 * <f(q2)>
        mu when rL = <f(q2)> * (4 + 2 * <rL>)
        mu when r7 = <f(q2)> * (2 * <r7>)
        mu when r1 = 2 * <f(q2)> * (1 + <r7> + <r1>)
        mu when r2 = 2 * <f(q2)> * (1 + <r7> + <r2>)
    Return:
        Ratio of <dG/dq^2> for mu and e for given wc_e, wc_mu and q2 values
    '''
    #get <dG/dq2> for electrons and muons for different q2 values
    fval_n = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_n, ll))  #Note that integrals are in units of GeV^2
    fval_d = np.array(kstll_obs_vals_intg(q2_min, q2_max, wc_d, ll)) 
    r_vals = fval_n/fval_d 
    return r_vals

def kstll_obs_vals_intg(q2_min, q2_max, wc_np, ll):
    #make q2 functions for dBR/dq2 for e 
    if ll == 'ee' or ll == 'mumu':
        return flavio.np_prediction('<dBR/dq2>(B0->K*'+ll+')', wc_np , q2min = q2_min, q2max = q2_max) 
        #return flavio.np_prediction('<dBR/dq2>(B+->K'+ll+')', wc_np , q2min = q2_min, q2max = q2_max) 
        ##return flavio.np_prediction('<dBR/dq2>(B0->K'+ll+')', wc_np , q2min = q2_min, q2max = q2_max) 
    else:
        raise Exception('The ll param not recognised', ll)

def plot_r_param(r_param_name, ylabel, xvals, yvals, labels, colors, fname_suffix, markers=None, y_scale = 'linear', bin_width = None):
    #make labels
    xlabel = '$q^{2} [GeV^{2}]$'
    if len(labels) != len(yvals):
        raise Exception('Length of labels do not match y values. Check!')

    #plot the result
    fig = plt.figure()
    ax  = fig.add_subplot(1, 1, 1)
    for i in range(len(yvals)): 
        if markers is not None:
            if bin_width is not None:
                ax.errorbar(xvals, yvals[i], xerr = 0.5 * bin_width, fmt = 'o', color=colors[i], label = labels[i])
            else:
                ax.scatter(xvals, yvals[i], marker=markers[i], color=colors[i], label = labels[i])
        else:
            if bin_width is not None:
                ax.errorbar(xvals, yvals[i], xerr = 0.5 * bin_width, fmt = 'o', color=colors[i], label = labels[i])
            else:
                ax.scatter(xvals, yvals[i], marker='o', color=colors[i], label = labels[i])

    if len(yvals) > 1: ax.legend(loc='best')
    ax.set_xlabel(xlabel, fontsize=20)
    ax.set_yscale(y_scale)
    ax.set_ylabel(ylabel, fontsize=20)
    if y_scale == 'log': ax.set_ylabel(ylabel + '(log)', fontsize=20)
    ax.axvspan(0.98, 1.10, alpha=0.5, color='red')
    ax.axvspan(8.00, 11.0, alpha=0.5, color='red')
    ax.axvspan(12.5, 15.0, alpha=0.5, color='red')
    fig.tight_layout()
    fig.savefig('./plots/'+r_param_name+'_'+fname_suffix+'.png')

def plot_r_param_mue_ratio(r_param_name, ylabel, xvals, yvals, fname_suffix):
    #make labels
    xlabel = '$q^{2} [GeV^{2}]$'
    #plot the result
    fig = plt.figure()
    ax  = fig.add_subplot(1, 1, 1)
    ax.scatter(xvals, yvals, marker='o', color='orange')
    ax.set_xlabel(xlabel, fontsize=20)
    ax.set_ylabel(ylabel, fontsize=20)
    #ax.axvspan(0.98, 1.10, alpha=0.5, color='red')
    ax.axvspan(8.00, 11.0, alpha=0.5, color='red')
    ax.axvspan(12.5, 15.0, alpha=0.5, color='red')
    fig.tight_layout()
    fig.savefig('./plots/'+r_param_name+'_'+fname_suffix+'.png')

def dGOdq2_gino(wc, par, sector, ll):
    #define a Gamma functions
    Gamma = lambda a: (C9 - a * C10)**2 + (C9p - a * C10p)**2 + rL * (C9 - a * C10) * (C9p - a * C10p) + C7 * (r7 * C7 + r1 * (C9 - a * C10) + r2 * (C9p - a * C10p))
    #get r_i
    rL    = np.array(par['rL'])
    r7    = np.array(par['r7'])
    r1    = np.array(par['r1'])
    r2    = np.array(par['r2'])
    #print(rL, r7, r1, r2)
    #get wc
    C7    = wc['C7eff_'+sector]      
    C9    = wc['C9_'+sector+ll]
    C9p   = wc['C9p_'+sector+ll]
    C10   = wc['C10_'+sector+ll]
    C10p  = wc['C10p_'+sector+ll]
    #print('Gino', sector, ll)
    #print('C7eff_'+sector  , wc['C7eff_'+sector])
    #print('C9_'+sector+ll  , wc['C9_'+sector+ll])
    #print('C9p_'+sector+ll , wc['C9p_'+sector+ll])
    #print('C10_'+sector+ll , wc['C10_'+sector+ll])
    #print('C10p_'+sector+ll, wc['C10p_'+sector+ll])
    #print(Gamma(1.) + Gamma(-1.))
    return Gamma(1.) + Gamma(-1.)

def get_rKst_gino(wilson_np, params, r_mu_dict, r_e_dict, scale = 4.8):
    #get SM + NP WCs
    wcs   = flavio.WilsonCoefficients() 
    wc_np = wilson_np.wc.dict
    wcs.set_initial(wc_np, scale = 4.8)
    wc_tot_mu  = wctot_dict(wc_obj=wcs, sector='bsmumu', scale=scale, par=params, nf_out=5)
    wc_tot_e   = wctot_dict(wc_obj=wcs, sector='bsee'  , scale=scale, par=params, nf_out=5)

    #dG/dq2 muons
    dgdq2_mu = dGOdq2_gino(wc_tot_mu, r_mu_dict, 'bs', 'mumu')
    dgdq2_e  = dGOdq2_gino(wc_tot_e, r_e_dict  , 'bs', 'ee')
    #make RKst
    rKst = dgdq2_mu/dgdq2_e
    return rKst

def get_rKst_gino_intg(wilson_np, params, r_mu_dict, r_e_dict, scale = 4.8):
    #get SM + NP WCs
    wcs   = flavio.WilsonCoefficients() 
    wc_np = wilson_np.wc.dict
    wcs.set_initial(wc_np, scale = 4.8)
    wc_tot_mu  = wctot_dict(wc_obj=wcs, sector='bsmumu', scale=scale, par=params, nf_out=5)
    wc_tot_e   = wctot_dict(wc_obj=wcs, sector='bsee'  , scale=scale, par=params, nf_out=5)

    #dG/dq2 muons
    dgdq2_mu = dGOdq2_gino(wc_tot_mu, r_mu_dict, 'bs', 'mumu')
    dgdq2_e  = dGOdq2_gino(wc_tot_e, r_e_dict  , 'bs', 'ee')
    #make RKst
    rKst = dgdq2_mu/dgdq2_e
    return rKst

def main():
    ##define masses, just to know the phase space boundary
    #me  = 0.5109e-3
    #mmu = 105.65e-3
    #mB  = 5279.65e-3
    #mV  = 895.5e-3
    #q2_lowe  = 4. * me**2 #2.6101881e-07 GeV^2
    #q2_lowmu = 4. * me**2 #0.0111619225 GeV^2
    #q2_max   = (mB - mV)**2 #19.2207712225 GeV^2

    #define four q2 ranges vetoeing phi, jpsi, psi(2S), these ranges are used in Kstmumu analysis
    q2_ranges  = []
    q2_ranges += [(0.10,0.98)]
    q2_ranges += [(1.10,8.00)]
    q2_ranges += [(1.00,8.00)]
    q2_ranges += [(11.0,12.5)]
    q2_ranges += [(15.0,19.0)]
    #range used in pK
    #q2_ranges += [(0.1  , 6.0)] #RpK
    #range used in Kpi and RKpipi
    #q2_ranges += [(1.1  , 7.0)] #RKpi and RKpipi

    #get SM WC
    wc_sm   = get_wilson_sm()
    #Set the new physics WC accordingly once for electron and muon (C9=1 and rest 0 for electron) to get r_i values
    wc_np_d_e = get_wilson_np(wc_sm, ll_den = 'ee')
    wc_np_d_m = get_wilson_np(wc_sm, ll_den = 'mumu')
    #Set the new physics WC accordingly once for electron and muon (here the settings are r name dependent), again used to obtain r_i values
    wc_np_num_rL_mu= get_wilson_np(wc_sm, ll_num = 'mumu', r_param_name='rL')
    wc_np_num_r7_mu= get_wilson_np(wc_sm, ll_num = 'mumu', r_param_name='r7')
    wc_np_num_r1_mu= get_wilson_np(wc_sm, ll_num = 'mumu', r_param_name='r1')
    wc_np_num_r2_mu= get_wilson_np(wc_sm, ll_num = 'mumu', r_param_name='r2')
    wc_np_num_rL_e = get_wilson_np(wc_sm, ll_num = 'ee', r_param_name='rL')
    wc_np_num_r7_e = get_wilson_np(wc_sm, ll_num = 'ee', r_param_name='r7')
    wc_np_num_r1_e = get_wilson_np(wc_sm, ll_num = 'ee', r_param_name='r1')
    wc_np_num_r2_e = get_wilson_np(wc_sm, ll_num = 'ee', r_param_name='r2')

    #make containers to put things q2 and r_i values in
    q2_vals = []
    fL_vals_mu = []
    rL_vals_mu = []
    r7_vals_mu = []
    r1_vals_mu = []
    r2_vals_mu = []
    fL_vals_e  = []
    rL_vals_e  = []
    r7_vals_e  = []
    r1_vals_e  = []
    r2_vals_e  = []
    #create points for q2
    n_pts = 100
    for q2r in q2_ranges:
        #make a q2 range and put into container
        q2v    = np.linspace(q2r[0], q2r[1], n_pts)
        q2_vals+= [q2v]
        ##get the rKst values that are related to r_i using muons in the numerator
        fL_rKst_mu = get_rKst_flavio_new(q2v, wc_np_d_m, wc_np_num_rL_mu, 'mumu', get_denominator_only = True)
        rL_rKst_mu = get_rKst_flavio_new(q2v, wc_np_d_m, wc_np_num_rL_mu, 'mumu')
        r7_rKst_mu = get_rKst_flavio_new(q2v, wc_np_d_m, wc_np_num_r7_mu, 'mumu')
        r1_rKst_mu = get_rKst_flavio_new(q2v, wc_np_d_m, wc_np_num_r1_mu, 'mumu')
        r2_rKst_mu = get_rKst_flavio_new(q2v, wc_np_d_m, wc_np_num_r2_mu, 'mumu')
        ##get the rKst values that are related to r_i using electrons in the numerator
        fL_rKst_e = get_rKst_flavio_new(q2v, wc_np_d_e, wc_np_num_rL_e, 'ee', get_denominator_only = True)
        rL_rKst_e = get_rKst_flavio_new(q2v, wc_np_d_e, wc_np_num_rL_e, 'ee')
        r7_rKst_e = get_rKst_flavio_new(q2v, wc_np_d_e, wc_np_num_r7_e, 'ee')
        r1_rKst_e = get_rKst_flavio_new(q2v, wc_np_d_e, wc_np_num_r1_e, 'ee')
        r2_rKst_e = get_rKst_flavio_new(q2v, wc_np_d_e, wc_np_num_r2_e, 'ee')
        #convert rKst values to actual r_i(q2) values with muons in the numerator
        fL_val_mu = fL_rKst_mu/2.
        rL_val_mu = convert_rrel_rval('rL', rL_rKst_mu)
        r7_val_mu = convert_rrel_rval('r7', r7_rKst_mu)
        r1_val_mu = convert_rrel_rval('r1', r1_rKst_mu, r7_vals = r7_rKst_mu)
        r2_val_mu = convert_rrel_rval('r2', r2_rKst_mu, r7_vals = r7_rKst_mu)
        #convert rKst values to actual r_i(q2) values with electors in the numerator
        fL_val_e = fL_rKst_e/2.
        rL_val_e = convert_rrel_rval('rL', rL_rKst_e)
        r7_val_e = convert_rrel_rval('r7', r7_rKst_e)
        r1_val_e = convert_rrel_rval('r1', r1_rKst_e, r7_vals = r7_rKst_e)
        r2_val_e = convert_rrel_rval('r2', r2_rKst_e, r7_vals = r7_rKst_e)
        #put them into containers
        fL_vals_mu += [fL_val_mu]
        rL_vals_mu += [rL_val_mu]
        r7_vals_mu += [r7_val_mu]
        r1_vals_mu += [r1_val_mu]
        r2_vals_mu += [r2_val_mu]
        fL_vals_e  += [fL_val_e]
        rL_vals_e  += [rL_val_e]
        r7_vals_e  += [r7_val_e]
        r1_vals_e  += [r1_val_e]
        r2_vals_e  += [r2_val_e]

    q2_vals     = np.concatenate(q2_vals)
    #for muons in numerator
    fL_vals_mu = np.concatenate(fL_vals_mu)
    rL_vals_mu = np.concatenate(rL_vals_mu)
    r7_vals_mu = np.concatenate(r7_vals_mu)
    r1_vals_mu = np.concatenate(r1_vals_mu)
    r2_vals_mu = np.concatenate(r2_vals_mu)
    #for electrons in numerator
    fL_vals_e  = np.concatenate(fL_vals_e)
    rL_vals_e  = np.concatenate(rL_vals_e)
    r7_vals_e  = np.concatenate(r7_vals_e)
    r1_vals_e  = np.concatenate(r1_vals_e)
    r2_vals_e  = np.concatenate(r2_vals_e)
    plot_r_param('rL_BToKstarll', ylabel='$r_{L}(q^2)$', xvals=q2_vals, yvals=[rL_vals_mu, rL_vals_e], labels=['$\mu$', 'e'], colors=['b', 'g'], fname_suffix='ee')
    plot_r_param('r7_BToKstarll', ylabel='$r_{7}(q^2)$', xvals=q2_vals, yvals=[r7_vals_mu, r7_vals_e], labels=['$\mu$', 'e'], colors=['b', 'g'], fname_suffix='ee')
    plot_r_param('r1_BToKstarll', ylabel='$r_{1}(q^2)$', xvals=q2_vals, yvals=[r1_vals_mu, r1_vals_e], labels=['$\mu$', 'e'], colors=['b', 'g'], fname_suffix='ee')
    plot_r_param('r2_BToKstarll', ylabel='$r_{2}(q^2)$', xvals=q2_vals, yvals=[r2_vals_mu, r2_vals_e], labels=['$\mu$', 'e'], colors=['b', 'g'], fname_suffix='ee')
    plot_r_param('fL_BToKstarll', ylabel='$f^{l}(q^2)$', xvals=q2_vals, yvals=[fL_vals_mu, fL_vals_e], labels=['$\mu$', 'e'], colors=['b', 'g'], fname_suffix='ee')

if __name__ == '__main__':
    main()
