#other libraries
from scipy.stats import chi2
from util import FitParameter, Minimize
import time 
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec
from scipy import special
import os
import pandas as pd
import pickle
import mplhep as hep

#plt.style.use(['science','ieee'])
#plt.style.use(hep.style.LHCb2)

#define comb types
COMB_TYPES = []
COMB_TYPES += ['combSM']
COMB_TYPES += ['comb1']
COMB_TYPES += ['comb2']
COMB_TYPES += ['comb3']
COMB_TYPES += ['comb4']
COMB_TYPES += ['comb5']
COMB_TYPES += ['comb6']
#define small r
SMALL_R = []
SMALL_R += ['rL_pK']    
SMALL_R += ['rL_Kpi']   
SMALL_R += ['rL_Kpipi'] 
SMALL_R += ['r7_pK']    
SMALL_R += ['r7_Kpi']   
SMALL_R += ['r7_Kpipi'] 
SMALL_R += ['r1_pK']    
SMALL_R += ['r1_Kpi']   
SMALL_R += ['r1_Kpipi'] 
SMALL_R += ['r2_pK']    
SMALL_R += ['r2_Kpi']   
SMALL_R += ['r2_Kpipi'] 
#define small r range
SMALL_R_RANGE = {}
SMALL_R_RANGE['rL_pK']     = (-2., 2.)
SMALL_R_RANGE['rL_Kpi']    = (-2., 2.) 
SMALL_R_RANGE['rL_Kpipi']  = (-2., 2.) 
SMALL_R_RANGE['r7_pK']     = (0., 111.)
SMALL_R_RANGE['r7_Kpi']    = (0., 55.)
SMALL_R_RANGE['r7_Kpipi']  = (0., 55.)
SMALL_R_RANGE['r1_pK']     = (-11., 11.)
SMALL_R_RANGE['r1_Kpi']    = (-11., 11.)
SMALL_R_RANGE['r1_Kpipi']  = (-11., 11.)
SMALL_R_RANGE['r2_pK']     = (-4., 4.)
SMALL_R_RANGE['r2_Kpi']    = (-4., 4.)  
SMALL_R_RANGE['r2_Kpipi']  = (-4., 4.) 
#define common WCs for all comb types
COMMON_WC = []
COMMON_WC += ['C9mu'        ]
COMMON_WC += ['C10mu'       ]
COMMON_WC += ['C9pmu'       ]
COMMON_WC += ['C10pmu'      ]
COMMON_WC += ['delta_CS_CSp']
#combination specific wcs and small rs
COMB_SPECIFIC_WC = {}
COMB_SPECIFIC_WC['RpKLHCb']  = {}
COMB_SPECIFIC_WC['LFU_RpKLHCb']  = {}
COMB_SPECIFIC_WC['LFU_RpKLHCbReducedUncertainty']  = {}
COMB_SPECIFIC_WC['LFU_RpK']  = {}
COMB_SPECIFIC_WC['LFU_RpKReducedUncertainty']  = {}
COMB_SPECIFIC_WC['LFU_RpKLHCbFixSmallr']  = {}
COMB_SPECIFIC_WC['RX'] = {}
COMB_SPECIFIC_WC['RXReducedUncertaintyr7pK']  = {}
COMB_SPECIFIC_WC['RXFixr7']  = {}
COMB_SPECIFIC_WC['RXFixAllr']  = {}
COMB_SPECIFIC_WC['RXWideRange']  = {}
for k in COMB_SPECIFIC_WC.keys():
    COMB_SPECIFIC_WC[k]['combSM'] = {}
    COMB_SPECIFIC_WC[k]['comb1']  = {}
    COMB_SPECIFIC_WC[k]['comb2']  = {}
    COMB_SPECIFIC_WC[k]['comb3']  = {}
    COMB_SPECIFIC_WC[k]['comb4']  = {}
    COMB_SPECIFIC_WC[k]['comb5']  = {}
    COMB_SPECIFIC_WC[k]['comb6']  = {}

    if k == 'RX' or k == 'RXReducedUncertaintyr7pK' or k == 'RXFixr7' or k == 'RXFixAllr' or k == 'RXWideRange':
        COMB_SPECIFIC_WC[k]['combSM']['False'] = ['C9mu']
        COMB_SPECIFIC_WC[k]['comb1' ]['False'] = COMMON_WC + ['deltaC9', 'deltaC10' ]
        COMB_SPECIFIC_WC[k]['comb2' ]['False'] = COMMON_WC + ['deltaC9', 'deltaC9p' ]
        COMB_SPECIFIC_WC[k]['comb3' ]['False'] = COMMON_WC + ['deltaC9', 'deltaC10p']
        COMB_SPECIFIC_WC[k]['comb4' ]['False'] = COMMON_WC + ['deltaC10','deltaC10p']
        COMB_SPECIFIC_WC[k]['comb5' ]['False'] = COMMON_WC + ['deltaC9p','deltaC10p']
        COMB_SPECIFIC_WC[k]['comb6' ]['False'] = COMMON_WC + ['deltaC9p','deltaC10' ]

        if k == 'RXFixr7':  
            small_r_wo_r7 = [r for r in SMALL_R if 'r7' not in r]
            COMB_SPECIFIC_WC[k]['combSM']['True']  = ['C9mu']  + small_r_wo_r7
            COMB_SPECIFIC_WC[k]['comb1' ]['True']  = COMMON_WC + ['deltaC9', 'deltaC10' ]  + small_r_wo_r7
            COMB_SPECIFIC_WC[k]['comb2' ]['True']  = COMMON_WC + ['deltaC9', 'deltaC9p' ]  + small_r_wo_r7
            COMB_SPECIFIC_WC[k]['comb3' ]['True']  = COMMON_WC + ['deltaC9', 'deltaC10p']  + small_r_wo_r7
            COMB_SPECIFIC_WC[k]['comb4' ]['True']  = COMMON_WC + ['deltaC10','deltaC10p']  + small_r_wo_r7
            COMB_SPECIFIC_WC[k]['comb5' ]['True']  = COMMON_WC + ['deltaC9p','deltaC10p']  + small_r_wo_r7
            COMB_SPECIFIC_WC[k]['comb6' ]['True']  = COMMON_WC + ['deltaC9p','deltaC10' ]  + small_r_wo_r7
        elif k == 'RXFixAllr':  
            COMB_SPECIFIC_WC[k]['combSM']['True']  =  COMB_SPECIFIC_WC[k]['combSM']['False']
            COMB_SPECIFIC_WC[k]['comb1' ]['True']  =  COMB_SPECIFIC_WC[k]['comb1' ]['False']
            COMB_SPECIFIC_WC[k]['comb2' ]['True']  =  COMB_SPECIFIC_WC[k]['comb2' ]['False']
            COMB_SPECIFIC_WC[k]['comb3' ]['True']  =  COMB_SPECIFIC_WC[k]['comb3' ]['False']
            COMB_SPECIFIC_WC[k]['comb4' ]['True']  =  COMB_SPECIFIC_WC[k]['comb4' ]['False']
            COMB_SPECIFIC_WC[k]['comb5' ]['True']  =  COMB_SPECIFIC_WC[k]['comb5' ]['False']
            COMB_SPECIFIC_WC[k]['comb6' ]['True']  =  COMB_SPECIFIC_WC[k]['comb6' ]['False']
        else:
            COMB_SPECIFIC_WC[k]['combSM']['True']  = ['C9mu']  + SMALL_R
            COMB_SPECIFIC_WC[k]['comb1' ]['True']  = COMMON_WC + ['deltaC9', 'deltaC10' ]  + SMALL_R
            COMB_SPECIFIC_WC[k]['comb2' ]['True']  = COMMON_WC + ['deltaC9', 'deltaC9p' ]  + SMALL_R
            COMB_SPECIFIC_WC[k]['comb3' ]['True']  = COMMON_WC + ['deltaC9', 'deltaC10p']  + SMALL_R
            COMB_SPECIFIC_WC[k]['comb4' ]['True']  = COMMON_WC + ['deltaC10','deltaC10p']  + SMALL_R
            COMB_SPECIFIC_WC[k]['comb5' ]['True']  = COMMON_WC + ['deltaC9p','deltaC10p']  + SMALL_R
            COMB_SPECIFIC_WC[k]['comb6' ]['True']  = COMMON_WC + ['deltaC9p','deltaC10' ]  + SMALL_R
    elif k == 'RpKLHCb':
        COMB_SPECIFIC_WC[k]['combSM']['False'] = ['C9mu']
        COMB_SPECIFIC_WC[k]['comb1' ]['False'] = COMMON_WC + ['deltaC9', 'deltaC10' ]
        COMB_SPECIFIC_WC[k]['comb2' ]['False'] = COMMON_WC + ['deltaC9', 'deltaC9p' ]
        COMB_SPECIFIC_WC[k]['comb3' ]['False'] = COMMON_WC + ['deltaC9', 'deltaC10p']
        COMB_SPECIFIC_WC[k]['comb4' ]['False'] = COMMON_WC + ['deltaC10','deltaC10p']
        COMB_SPECIFIC_WC[k]['comb5' ]['False'] = COMMON_WC + ['deltaC9p','deltaC10p']
        COMB_SPECIFIC_WC[k]['comb6' ]['False'] = COMMON_WC + ['deltaC9p','deltaC10' ]

        small_r_rpklhcb = [smr for smr in SMALL_R if 'pK' in smr]
        COMB_SPECIFIC_WC[k]['combSM']['True']  = ['C9mu']  + small_r_rpklhcb
        COMB_SPECIFIC_WC[k]['comb1' ]['True']  = COMMON_WC + ['deltaC9', 'deltaC10' ]  + small_r_rpklhcb
        COMB_SPECIFIC_WC[k]['comb2' ]['True']  = COMMON_WC + ['deltaC9', 'deltaC9p' ]  + small_r_rpklhcb
        COMB_SPECIFIC_WC[k]['comb3' ]['True']  = COMMON_WC + ['deltaC9', 'deltaC10p']  + small_r_rpklhcb
        COMB_SPECIFIC_WC[k]['comb4' ]['True']  = COMMON_WC + ['deltaC10','deltaC10p']  + small_r_rpklhcb
        COMB_SPECIFIC_WC[k]['comb5' ]['True']  = COMMON_WC + ['deltaC9p','deltaC10p']  + small_r_rpklhcb
        COMB_SPECIFIC_WC[k]['comb6' ]['True']  = COMMON_WC + ['deltaC9p','deltaC10' ]  + small_r_rpklhcb

    if 'LFU' in k:
        COMB_SPECIFIC_WC[k]['combSM']['False'] = []
        COMB_SPECIFIC_WC[k]['comb1' ]['False'] = ['deltaC9', 'deltaC10' ]
        COMB_SPECIFIC_WC[k]['comb2' ]['False'] = ['deltaC9', 'deltaC9p' ]
        COMB_SPECIFIC_WC[k]['comb3' ]['False'] = ['deltaC9', 'deltaC10p']
        COMB_SPECIFIC_WC[k]['comb4' ]['False'] = ['deltaC10','deltaC10p']
        COMB_SPECIFIC_WC[k]['comb5' ]['False'] = ['deltaC9p','deltaC10p']
        COMB_SPECIFIC_WC[k]['comb6' ]['False'] = ['deltaC9p','deltaC10' ]

        small_r_rpklhcb = [smr for smr in SMALL_R if 'pK' in smr]
        if 'Fix' not in k:
            COMB_SPECIFIC_WC[k]['combSM']['True']  = small_r_rpklhcb
            COMB_SPECIFIC_WC[k]['comb1' ]['True']  = ['deltaC9', 'deltaC10' ]  + small_r_rpklhcb
            COMB_SPECIFIC_WC[k]['comb2' ]['True']  = ['deltaC9', 'deltaC9p' ]  + small_r_rpklhcb
            COMB_SPECIFIC_WC[k]['comb3' ]['True']  = ['deltaC9', 'deltaC10p']  + small_r_rpklhcb
            COMB_SPECIFIC_WC[k]['comb4' ]['True']  = ['deltaC10','deltaC10p']  + small_r_rpklhcb
            COMB_SPECIFIC_WC[k]['comb5' ]['True']  = ['deltaC9p','deltaC10p']  + small_r_rpklhcb
            COMB_SPECIFIC_WC[k]['comb6' ]['True']  = ['deltaC9p','deltaC10' ]  + small_r_rpklhcb
        else:
            COMB_SPECIFIC_WC[k]['combSM']['True']  = []
            COMB_SPECIFIC_WC[k]['comb1' ]['True']  = ['deltaC9', 'deltaC10' ] 
            COMB_SPECIFIC_WC[k]['comb2' ]['True']  = ['deltaC9', 'deltaC9p' ]
            COMB_SPECIFIC_WC[k]['comb3' ]['True']  = ['deltaC9', 'deltaC10p']
            COMB_SPECIFIC_WC[k]['comb4' ]['True']  = ['deltaC10','deltaC10p']
            COMB_SPECIFIC_WC[k]['comb5' ]['True']  = ['deltaC9p','deltaC10p']
            COMB_SPECIFIC_WC[k]['comb6' ]['True']  = ['deltaC9p','deltaC10' ]

def cost_func_chi2(params, *args):
    #get data
    data  = args[0]
    #get params
    ddof  = params['ddof']
    if 'loc' in params:
        loc   = params['loc']
        scale = params['scale']
        #define fit chisq
        chisq = chi2(ddof, loc=loc, scale=scale)
    else:
        chisq = chi2(ddof)

    #get pdf values replacing zeros with a small tolerance so that logpdf is not undefined
    pdf   = chisq.pdf(data)
    pdf   = np.where( pdf == 0, 1e-20 * np.ones_like(pdf), pdf)
    logpdf= np.log(pdf)
    #print(params)
    #print(data)
    #print(logpdf)
    #print(np.sum(logpdf))
    return -2.* np.sum(logpdf)

def plot_chisq(data, fit_results, chisq_data, x_range = (0., 35.), fname = 'plots/chisq.pdf', fit_dof = None, plot_davide = False):
    #defines plot properties
    nbins = 50
    xmin, xmax = x_range
    #ngen = 50*len(data)
    ymax_shift = 120
    yscale = 'log'
    xlabel = r'-2 \times \Delta \log(L)$'
    ylabel = r'$Prob.$'
    plot_pull = False

    #define the chisq distribution from fit results
    ddof = fit_results['params']['ddof'][0]
    loc  = fit_results['params']['loc'][0]
    scale= fit_results['params']['scale'][0]
    chisq_fit = chi2(ddof, loc=loc, scale=scale)

    #davide LEE paper
    chisq_davide = chi2(5.5882, loc=0.067, scale=1.153)

    #evalue p-value and gaussian signficance
    pval  = 1. - chisq_fit.cdf(chisq_data)
    gsign = np.sqrt(2.)*special.erfcinv(pval)
    print('pvalue: {0}'.format(pval))
    print('Gaussian signif.: {0:.2f}'.format(gsign))
    #pval2 = 1. - chi2.cdf(chisq_data, df=ddof, loc=loc, scale=scale)
    #gsign2=chi2.ppf(1-pval2,1)**0.5
    #print('pvalue 2: {0}'.format(pval2))
    #print('Gaussian signif. 2: {0:.2f}'.format(gsign2))

    #make histograms and get bin edges
    cdata, bedges = np.histogram(data,  bins = nbins, range=(xmin, xmax), weights = None)
    bin_width     = bedges[1:] - bedges[:-1]
    bin_centers   = bedges[:-1] + 0.5 * bin_width
    #use bin_center
    fitres        = chisq_fit.pdf(bin_centers)
    fitres_davide = chisq_davide.pdf(bin_centers)
    #use integrated in bin
    #fakedata      = np.random.uniform(xmin, xmax, size=ngen)
    #np_model      = chisq_fit.pdf(fakedata)
    #fitres,_      = np.histogram(fakedata, bins = nbins, range=(xmin, xmax), weights=np_model)
    tot_yld       = len(data)
    fitres        = tot_yld * fitres/fitres.sum()
    fitres_davide = tot_yld * fitres_davide/fitres_davide.sum()

    #define pull
    dnm  = np.sqrt(fitres + cdata)
    nm   = cdata - fitres
    pull = np.divide(nm, dnm, out=np.zeros_like(nm), where=dnm!=0.)
    pull = np.nan_to_num(pull)

    #plot
    fig         = plt.figure()
    axs = None
    if plot_pull:
        gs  = gridspec.GridSpec(2, 1, height_ratios = [4,1])
        axs = plt.subplot(gs[0]), plt.subplot(gs[1])
    else:
        axs = (fig.add_subplot(111),)

    axs[0].errorbar(bin_centers, cdata.flatten(), xerr=0.5*bin_width, yerr=(np.sqrt(cdata)).flatten(), fmt = '.', color = 'black', label='Toys', linewidth = 2.0, markersize=9)
    axs[0].plot(bin_centers, fitres.flatten(), label='Fit', color='b', linewidth = 2.5)
    if plot_davide: axs[0].plot(bin_centers, fitres_davide.flatten(), label='LEE paper', color='pink', linewidth = 2.5)
    cmax = [np.max(cdata), np.max(fitres)]
    cmin = [np.min(cdata), np.min(fitres)]
    axs[0].legend(loc='best', fontsize=20)
    axs[0].set_xlim((xmin,xmax))
    ymin = max(min(cmin), 1e-10)
    ymax = max(cmax)+ymax_shift
    axs[0].axvline(x=chisq_data, ymin=ymin, ymax=ymax, color='r', label='Data')
    axs[0].set_xlabel(xlabel, fontsize=20, fontname = 'Times New Roman')
    axs[0].set_ylabel(ylabel, fontsize=20, fontname = 'Times New Roman')
    #axs[0].set_ylim((ymin,ymax))
    axs[0].set_yscale(yscale)

    #show the chisq/ndof on the fit
    chisq_ndof = np.sum(pull**2)/(nbins+fit_dof)
    print('Chisq/ndof', chisq_ndof)
    s = r'Gaus. Sig.= {0:.2f}'.format(gsign)
    axs[0].text(0.2,0.6,s,horizontalalignment='center',verticalalignment='center', transform=axs[0].transAxes, fontsize=13)
    axs[0].legend(loc='best')

    if plot_pull:
        #pull
        axs[1].hist(bedges[:-1], bedges, weights=pull.flatten(), color='green', range=(xmin, xmax))
        axs[1].hlines(-2.5, xmin, xmax, color='r', linestyles='--', alpha=0.5)
        axs[1].hlines( 2.5, xmin, xmax, color='r', linestyles='--', alpha=0.5)
        axs[1].hlines(-5., xmin, xmax, color='r', linestyles='-', alpha=0.5)
        axs[1].hlines( 5., xmin, xmax, color='r', linestyles='-', alpha=0.5)
        axs[1].set_ylabel('Pull')
        axs[1].set_xlim((xmin,xmax))
        axs[1].set_ylim((np.min(pull),np.max(pull)))

    fig.tight_layout()
    fig.savefig(fname)
    fig.clf()

def plot_chisq_both(data, fit_results, chisq_data, data_rx, fit_results_rx, chisq_data_rx, x_range = (0., 50.), fname = 'plots/chisq.pdf', prefix = 'RX'):
    #defines plot properties
    nbins = 50
    xmin, xmax = x_range
    ymax_shift = 120
    yscale = 'log'
    xlabel = r'$-2 \times \Delta \log(L)$'
    ylabel = r'$Prob.$'

    #define the chisq distribution from fit results
    ddof = fit_results['params']['ddof'][0]
    loc  = fit_results['params']['loc'][0]
    scale= fit_results['params']['scale'][0]
    chisq_fit = chi2(ddof, loc=loc, scale=scale)
    #evalue p-value and gaussian signficance
    pval  = 1. - chisq_fit.cdf(chisq_data)
    gsign = np.sqrt(2.)*special.erfcinv(pval)
    print('pvalue: {0}'.format(pval))
    print('Gaussian signif.: {0:.2f}'.format(gsign))

    #define the chisq distribution from fit results
    ddof_rx = fit_results_rx['params']['ddof'][0]
    loc_rx  = fit_results_rx['params']['loc'][0]
    scale_rx= fit_results_rx['params']['scale'][0]
    chisq_fit_rx = chi2(ddof_rx, loc=loc_rx, scale=scale_rx)
    #evalue p-value and gaussian signficance
    pval_rx  = 1. - chisq_fit_rx.cdf(chisq_data_rx)
    gsign_rx = np.sqrt(2.)*special.erfcinv(pval_rx)
    print('pvalue: {0}'.format(pval_rx))
    print('Gaussian signif.: {0:.2f}'.format(gsign_rx))
    if prefix == 'RX':
        chisq_data_rx_1 = 31.50698706276927
        pval_rx_1  = 1. - chisq_fit_rx.cdf(chisq_data_rx_1)
        gsign_rx_1 = np.sqrt(2.)*special.erfcinv(pval_rx_1)
        print('pvalue (RX=1): {0}'.format(pval_rx_1))
        print('Gaussian signif. (RX=1): {0:.2f}'.format(gsign_rx_1))

    #print(chisq_data, chisq_data_rx, chisq_data_rx_1)
    #make histograms
    cdata, bedges = np.histogram(data,   bins = nbins, range=(xmin, xmax), weights = None)
    bin_width     = bedges[1:] - bedges[:-1]
    bin_centers   = bedges[:-1] + 0.5 * bin_width
    fitres        = chisq_fit.pdf(bin_centers)
    fitres        = len(data) * fitres/fitres.sum()

    #make histograms
    cdata_rx, _   = np.histogram(data_rx,bins = nbins, range=(xmin, xmax), weights = None)
    fitres_rx     = chisq_fit_rx.pdf(bin_centers)
    fitres_rx     = len(data_rx) * fitres_rx/fitres_rx.sum()

    #plot
    fig = plt.figure()
    axs = (fig.add_subplot(111),)

    #axs[0].errorbar(bin_centers, cdata.flatten()   , xerr=0.5*bin_width, yerr=(np.sqrt(cdata)).flatten(), fmt = '.', color = 'red', linewidth = 2.0, markersize=9, label = 'SM expts w/o $R_{X}$'+prefix)
    #axs[0].plot(bin_centers, fitres.flatten(), label='Fit w/o '+prefix, color='r', linestyle = '-', linewidth = 2.5)
    axs[0].errorbar(bin_centers, cdata.flatten()   , xerr=0.5*bin_width, yerr=(np.sqrt(cdata)).flatten(), fmt = '.', color = 'red', linewidth = 2.0, markersize=9, label = 'SM expts')
    axs[0].plot(bin_centers, fitres.flatten(), color='r', linestyle = '-', linewidth = 2.5)

    #axs[0].errorbar(bin_centers, cdata_rx.flatten(), xerr=0.5*bin_width, yerr=(np.sqrt(cdata_rx)).flatten(), fmt = '.', color = 'green', linewidth = 2.0, markersize=9, label = 'SM pseudo-expt. '+prefix)
    #axs[0].plot(bin_centers, fitres_rx.flatten(), label='Fit with '+prefix, color='green', linestyle = '--', linewidth = 2.5)
    if 'RX' in prefix:
        axs[0].errorbar(bin_centers, cdata_rx.flatten(), xerr=0.5*bin_width, yerr=(np.sqrt(cdata_rx)).flatten(), fmt = '.', color = 'blue', linewidth = 2.0, markersize=9, label = 'SM expts (incld $R_{X}$)')
    else:
        axs[0].errorbar(bin_centers, cdata_rx.flatten(), xerr=0.5*bin_width, yerr=(np.sqrt(cdata_rx)).flatten(), fmt = '.', color = 'blue', linewidth = 2.0, markersize=9, label = 'SM expts (incld $R_{pK}$)')

    axs[0].plot(bin_centers, fitres_rx.flatten(), color='blue', linestyle = '-', linewidth = 2.5)
    
    cmax = [np.max(cdata), np.max(fitres), np.max(cdata_rx), np.max(fitres_rx)]
    cmin = [np.min(cdata), np.min(fitres), np.min(cdata_rx), np.min(fitres_rx)]
    ymin = max(min(cmin), 1e-10)
    ymax = max(cmax)+ymax_shift
    #axs[0].axvline(x=chisq_data, ymin=ymin, ymax=ymax, color='red', linestyle = '--', label='Data w/o '+prefix)
    #axs[0].axvline(x=chisq_data_rx, ymin=ymin, ymax=ymax, color='green', linestyle = '--', label='Data with '+prefix+'=0.8')
    axs[0].axvline(x=chisq_data, ymin=ymin, ymax=ymax, color='red', linestyle = '--', label='Data')
    if 'RX' in prefix:
        axs[0].axvline(x=chisq_data_rx, ymin=ymin, ymax=ymax, color='blue', linestyle = '--', label='Data + $R_{X}$ = 0.8')
    else:
        axs[0].axvline(x=chisq_data_rx, ymin=ymin, ymax=ymax, color='blue', linestyle = '--', label='Data + LHCb $R_{pK}$')

    #if prefix == 'RX': 
    #    axs[0].axvline(x=chisq_data_rx_1, ymin=ymin, ymax=ymax, color='blue', linestyle = '--', label='Data with '+prefix+'=1')
    #    s3 = r'{0:.2f} $\sigma$ (with RX=1)'.format(gsign_rx_1)


    #s1 = r'{0:.2f} $\sigma$ (w/o {1})'.format(gsign, prefix)
    #s2 = r'{0:.2f} $\sigma$ (with {1})'.format(gsign_rx, prefix)
    s1 = r'{0:.2f} $\sigma$ (w/o)'.format(gsign)
    s2 = r'{0:.2f} $\sigma$ (with RX=0.8)'.format(gsign_rx)

    leg = axs[0].legend(loc='best', fontsize=16, framealpha=1.)
    axs[0].set_xlim((xmin,xmax))
    axs[0].set_xlabel(xlabel, fontsize=20, fontname='Times New Roman')
    axs[0].set_ylabel(ylabel, fontsize=20, fontname='Times New Roman')
    axs[0].set_yscale(yscale)
    #for lh in leg.legendHandles: lh.set_alpha(1)
    #axs[0].text(0.3,0.6,s1,horizontalalignment='center',verticalalignment='center', transform=axs[0].transAxes, fontsize=13)
    #axs[0].text(0.3,0.5,s2,horizontalalignment='center',verticalalignment='center', transform=axs[0].transAxes, fontsize=13)
    #if prefix == 'RX': axs[0].text(0.3,0.4,s3,horizontalalignment='center',verticalalignment='center', transform=axs[0].transAxes, fontsize=13)
    #axs[0].legend(loc='best')
    fig.tight_layout()
    fig.savefig(fname.replace('.pdf', '_'+prefix+'.pdf'))
    fig.clf()

def get_comb_fitres(combination_type, max_seed, rx_include = 'False', prefix = 'RX', suffix = 'toy', failed_toys = None):
    print(combination_type, rx_include, suffix, prefix)
    #make a dictionary to hold all the results
    comb_res = {}
    comb_res['is_valid'] = []
    comb_res['loglh']    = []
    if len(COMB_SPECIFIC_WC[prefix][combination_type][rx_include]) != 0: 
        comb_res['minimizer']= []
        for k in COMB_SPECIFIC_WC[prefix][combination_type][rx_include]: 
            comb_res[k]= []
            comb_res[k+'_err']= []

    #loop over toy index to get the results
    for toy_index in range(max_seed):
        #print(toy_index)
        res   = {}
        fname = '/home/hep/amathad/Packages/flavio-globalwcfit-rx-inclusion/fitresults_'+prefix+'/result_'+str(toy_index)+'_'+combination_type+'_rx'+str(rx_include)+'_'+suffix+'.p' 
        if os.path.isfile(fname):
            #if fit results exists go get it
            res   = pickle.load( open( fname, "rb" ) )

            #a work around for the LFU_RpKLHCbFixSmallr case
            if prefix == 'LFU_RpKLHCbFixSmallr':
                if 'params' in res:
                    res['params'].pop('rL_pK', None)
                    res['params'].pop('r7_pK', None)
                    res['params'].pop('r1_pK', None)
                    res['params'].pop('r2_pK', None)

            #if minima invalid add nan to entry
            if type(res['is_valid']) == int and res['is_valid'] == 0:
                if suffix != 'data':
                    print('Fit is invalid for', fname)
                    #print(toy_index)
                    failed_toys += [toy_index]
                    res['loglh'] = np.nan
                    res['minimizer'] = np.nan
                    if len(COMB_SPECIFIC_WC[prefix][combination_type][rx_include]) != 0: 
                        for k in COMB_SPECIFIC_WC[prefix][combination_type][rx_include]: res['params'][k]= (np.nan, np.nan)
                else:
                    raise Exception('Minima is invalid for the data fit result for comb type:', combination_type, 'and rx_include:', rx_include, '. Please check!')
            else:
                #print toy index where MIGRAD errors are nan
                if len(COMB_SPECIFIC_WC[prefix][combination_type][rx_include]) != 0: 
                    if res['minimizer'] == 'MIGRAD':
                        for k in COMB_SPECIFIC_WC[prefix][combination_type][rx_include]: 
                            if np.isnan(res['params'][k][1]): 
                                print('Fit has parameter',k,' with error that is nan', fname)
                                #print(toy_index)
                                failed_toys += [toy_index]
                                if suffix == 'data': raise Exception('Data has a parameter with error as nan from MIGRAD. Please Check!')

            if len(COMB_SPECIFIC_WC[prefix][combination_type][rx_include]) != 0: 
                #if minimizer is SLQPS set error to nan but keep the central value
                if res['minimizer'] == 'SLSQP':
                    if len(COMB_SPECIFIC_WC[prefix][combination_type][rx_include]) != 0: 
                        for k in COMB_SPECIFIC_WC[prefix][combination_type][rx_include]: res['params'][k]= (res['params'][k][0], np.nan)
        else:
            #if results does not exist make a fake invalid entry for toy only (will be removed in the final chisq fit)
            print('Fit result does not exist', fname)
            failed_toys += [toy_index]
            if suffix != 'data':
                res['is_valid'] = 0
                res['loglh'] = np.nan
                res['minimizer'] = np.nan
                res['params'] = {}
                if len(COMB_SPECIFIC_WC[prefix][combination_type][rx_include]) != 0: 
                    for k in COMB_SPECIFIC_WC[prefix][combination_type][rx_include]: res['params'][k]= (np.nan, np.nan)
            else:
                raise Exception('Could not find the fit resutls for data for comb type:', combination_type, 'and rx_include:', rx_include, '. Please check!')

        comb_res['is_valid']  += [res['is_valid']]
        comb_res['loglh']     += [res['loglh']]
        if len(COMB_SPECIFIC_WC[prefix][combination_type][rx_include]) != 0: 
            comb_res['minimizer'] += [res['minimizer']]
            for k in res['params'].keys(): 
                comb_res[k] += [res['params'][k][0]]
                comb_res[k+'_err'] += [res['params'][k][1]]

    return pd.DataFrame(comb_res)

def plot_smallr_toy(comb_toys_True, comb_data_True, comb_type, prefix):
    variables =  [var for var in COMB_SPECIFIC_WC[prefix][comb_type]['True'] if ('rL_' in var) or ('r7_' in var) or ('r1_' in var) or ('r2_' in var)]
    for var in variables:
        print('Plotting {0} for {1}'.format(var, comb_type))
        #toy value
        arr_True       = comb_toys_True[var].to_numpy()
        mean_arr_True  = np.mean(arr_True)
        std_arr_True   = np.std(arr_True)
        #get data value
        data_val_True  = comb_data_True[var].to_numpy()[0]
        data_err_True  = comb_data_True[var+'_err'].to_numpy()[0]

        #plot
        fig, ax   = plt.subplots()
        ax.hist(arr_True , range = SMALL_R_RANGE[var], bins=100, alpha = 1.0, color='g', label = comb_type+' toys (RX included)', density = True)
        ax.annotate(var+'={0:.2f}$\pm${1:.2f}'.format(mean_arr_True, std_arr_True)   , alpha = 1.0, color='g', xy=(0.05, 0.90), xycoords='axes fraction')

        ax.axvline(x=data_val_True, color = 'm', label = comb_type+' data (RX included)')
        #ax.axvspan(data_val_True-data_err_True, data_val_True+data_err_True, facecolor='m', alpha=0.4, label = comb_type+' data (RX included)')
        ax.annotate(var+'={0:.2f}$\pm${1:.2f}'.format(data_val_True, data_err_True)  , alpha = 1.0, color='m', xy=(0.05, 0.85), xycoords='axes fraction')

        ax.legend(loc='upper right')
        ax.set_xlabel(var)
        ax.set_ylabel('Freq')
        fig.tight_layout()
        fig.savefig('plots/'+comb_type+'_'+var+'.pdf')
        plt.close('all')

def plot_WCs_toys(comb_toys_False, comb_toys_True, comb_data_False, comb_data_True, comb_type, prefix):
    if len(COMB_SPECIFIC_WC[prefix][comb_type]['False']) == 0: 
        print('Nothing to plot!')
        return None

    variables =  COMB_SPECIFIC_WC[prefix][comb_type]['False']
    for var in variables:
        print('Plotting {0} for {1}'.format(var, comb_type))
        #rx = False
        arr_False      = comb_toys_False[var].to_numpy()
        mean_arr_False = np.mean(arr_False)
        std_arr_False  = np.std(arr_False)
        #rx = True
        arr_True       = comb_toys_True[var].to_numpy()
        mean_arr_True  = np.mean(arr_True)
        std_arr_True   = np.std(arr_True)
        #get data val
        data_val_False = comb_data_False[var].to_numpy()[0]
        data_err_False = comb_data_False[var+'_err'].to_numpy()[0]
        data_val_True  = comb_data_True[var].to_numpy()[0]
        data_err_True  = comb_data_True[var+'_err'].to_numpy()[0]

        fig, ax   = plt.subplots()

        xmax = max(np.max(arr_False), np.max(arr_True))
        xmin = min(np.min(arr_False), np.min(arr_True))
        ax.hist(arr_False, range = (xmin, xmax), bins=100, alpha = 1.0, color='b', label = comb_type+' toys (RX not included)', density = True)
        ax.hist(arr_True , range = (xmin, xmax), bins=100, alpha = 1.0, color='g', label = comb_type+' toys (RX included)', density = True)
        ax.annotate(var+'={0:.2f}$\pm${1:.2f}'.format(mean_arr_False, std_arr_False) , alpha = 1.0, color='b', xy=(0.05, 0.95), xycoords='axes fraction')
        ax.annotate(var+'={0:.2f}$\pm${1:.2f}'.format(mean_arr_True, std_arr_True)   , alpha = 1.0, color='g', xy=(0.05, 0.90), xycoords='axes fraction')

        ax.axvline(x=data_val_False, color = 'saddlebrown')
        ax.axvspan(data_val_False-data_err_False, data_val_False+data_err_False, facecolor='saddlebrown', alpha=0.4, label = comb_type+' data (RX not included)')
        ax.annotate(var+'={0:.2f}$\pm${1:.2f}'.format(data_val_False, data_err_False), alpha = 1.0, color='saddlebrown', xy=(0.05, 0.85), xycoords='axes fraction')

        ax.axvline(x=data_val_True, color = 'm')
        ax.axvspan(data_val_True-data_err_True, data_val_True+data_err_True, facecolor='m', alpha=0.4, label = comb_type+' data (RX included)')
        ax.annotate(var+'={0:.2f}$\pm${1:.2f}'.format(data_val_True, data_err_True)  , alpha = 1.0, color='m', xy=(0.05, 0.80), xycoords='axes fraction')

        ax.legend(loc='upper right')
        ax.set_xlabel(var)
        ax.set_ylabel('Freq')
        #ax.set_yscale('log')
        fig.tight_layout()
        fig.savefig('plots/'+comb_type+'_'+var+'.pdf')
        plt.close('all')

def get_fitresults(prefix_True):

    ###########
    #define a helper function to get the fit results
    def _get_combs(mxseed, rx_include, prefix, suffix, make_fresh = True):
        fname = './pickled_fitres_new/combs_rx'+rx_include+'_'+prefix+'_'+suffix+'.p'
        combs = None
        failed_toys = []
        if os.path.isfile(fname) and not make_fresh: 
            print('Get the pickled results!')
            combs = pickle.load( open( fname, "rb" ) )
        else:
            print('Get the results from fit results!')
            combs = [get_comb_fitres(comb_type, mxseed, rx_include = rx_include, prefix = prefix, suffix = suffix, failed_toys = failed_toys) for comb_type in COMB_TYPES]
            pickle.dump(combs, open( fname, "wb" ) )

        return combs, failed_toys

    #given all comb fit results remove entries in all combs if even one of the entry is invalid
    def _get_valid_entries(combs, rx_include, prefix):
        print('Before cuts on toy results')
        for i, comb in enumerate(combs): print(COMB_TYPES[i], rx_include, comb.shape)

        #get rid of invalid minima
        cond = (combs[0]['is_valid'] == 1).to_numpy()
        for comb in combs[1:]: cond  = np.logical_and(cond, (comb['is_valid'] == 1).to_numpy())

        print('After cut on is_valid == 1')
        for i, comb in enumerate(combs): print(COMB_TYPES[i], rx_include, comb[cond].shape)
        
        #get rid of WCs that take crazy values
        #combs = [comb[cond] for comb in combs] #uncomment if inspecting individual WC
        for i, comb in enumerate(combs): 
            for k in COMB_SPECIFIC_WC[prefix][COMB_TYPES[i]][rx_include]: 
                if 'C' in k: 
                    #print(k)
                    cond  = np.logical_and(cond, np.abs(comb[k].to_numpy()) < 50)
                    #cond1  = np.abs(comb[k].to_numpy()) > 50 #uncomment if inspecting individual WC
                    #print(comb[cond1]) #uncomment if inspecting individual WC

        print('After cut on abs(WCs) < 50')
        for i, comb in enumerate(combs): print(COMB_TYPES[i], rx_include, comb[cond].shape)

        #combs = [comb[cond] for comb in combs] #uncomment if inspecting individual WC
        ##get rid of pos loglh
        #for comb in combs: 
        #    cond1  = comb['loglh'] > 0
        #    print(comb[cond1])
        #    print(comb[cond1].shape)

        return [comb[cond] for comb in combs]
    ###########

    #define number of toys for data and toy
    max_seed_toy   = 5001
    max_seed_data  = 1
    toy_res  = []
    data_res = []
    fnames   = []

    ###########
    if 'LFU' in prefix_True:
        prefix_False   = 'LFU_RpKLHCb'
    else:
        prefix_False   = 'RX'

    #get all the combs according to the order in COMB_TYPES
    combs_rxFalse_toy , failed_rxFalse_toy =  _get_combs(max_seed_toy, 'False', prefix_False, 'toy', make_fresh = False)
    combs_rxFalse_data, _  =  _get_combs(max_seed_data, 'False', prefix_False, 'data', make_fresh = False)
    if len(failed_rxFalse_toy) != 0:
        print(list(set(failed_rxFalse_toy)))
        pickle.dump(list(set(failed_rxFalse_toy)), open( 'job_submission_scripts/failed_rxFalse_toy_'+prefix_False+'.p', "wb" ) )

    #remove invalid entries from all
    combs_rxFalse_toy  = _get_valid_entries(combs_rxFalse_toy , 'False', prefix_False)
    combs_rxFalse_data = _get_valid_entries(combs_rxFalse_data, 'False', prefix_False)
    #print(combs_rxFalse_data[0])

    #make chisq arrays with nrows = nentries and ncolumns = 6 combs
    delta_chisq_False_toy   = np.array([( combs_rxFalse_toy[0]['loglh'] - comb['loglh']).to_numpy() for comb in combs_rxFalse_toy[1:] ]).T
    delta_chisq_False_data  = np.array([(combs_rxFalse_data[0]['loglh'] - comb['loglh']).to_numpy() for comb in combs_rxFalse_data[1:]]).T

    #calculate delta chisq 
    max_delta_chisq_False_toy   = np.max(delta_chisq_False_toy , axis=1)
    max_delta_chisq_False_data  = np.max(delta_chisq_False_data, axis=1)
    #print(max_delta_chisq_False_data)

    #return results
    toy_res  += [max_delta_chisq_False_toy]
    data_res += [max_delta_chisq_False_data[0]]
    fnames   += ['plots/chisq_rxFalse_'+prefix_False+'.pdf']
    ###########

    ###########
    #get all the combs according to the order in COMB_TYPES
    combs_rxTrue_toy  , failed_rxTrue_toy  =  _get_combs(max_seed_toy, 'True' , prefix_True, 'toy', make_fresh = False)
    combs_rxTrue_data , _  =  _get_combs(max_seed_data, 'True' , prefix_True, 'data', make_fresh = False)
    if len(failed_rxTrue_toy) != 0:
        print(list(set(failed_rxTrue_toy)))
        pickle.dump(list(set(failed_rxTrue_toy)), open( 'job_submission_scripts/failed_rxTrue_toy_'+prefix_True+'.p', "wb" ) )

    #remove invalid entries from all
    combs_rxTrue_toy   = _get_valid_entries(combs_rxTrue_toy  , 'True', prefix_True)
    combs_rxTrue_data  = _get_valid_entries(combs_rxTrue_data , 'True', prefix_True)
    print(combs_rxTrue_toy)
    print(combs_rxTrue_data)

    #make chisq arrays with nrows = nentries and ncolumns = 6 combs
    delta_chisq_True_toy    = np.array([(  combs_rxTrue_toy[0]['loglh'] - comb['loglh']).to_numpy() for comb in combs_rxTrue_toy[1:]  ]).T
    delta_chisq_True_data   = np.array([( combs_rxTrue_data[0]['loglh'] - comb['loglh']).to_numpy() for comb in combs_rxTrue_data[1:] ]).T

    #calculate delta chisq 
    max_delta_chisq_True_toy    = np.max(delta_chisq_True_toy  , axis=1)
    max_delta_chisq_True_data   = np.max(delta_chisq_True_data , axis=1)
    #print(max_delta_chisq_True_toy)
    #print(max_delta_chisq_True_data)

    toy_res  += [max_delta_chisq_True_toy]
    data_res += [max_delta_chisq_True_data[0]]
    fnames   += ['plots/chisq_rxTrue_'+prefix_True+'.pdf']
    ###########

    ###########
    ##plot toy resutls for small r
    #plot_smallr_toy(combs_rxTrue_toy[0], combs_rxTrue_data[0], 'combSM', prefix_True)
    #plot_smallr_toy(combs_rxTrue_toy[1], combs_rxTrue_data[1], 'comb1' , prefix_True)
    #plot_smallr_toy(combs_rxTrue_toy[2], combs_rxTrue_data[2], 'comb2' , prefix_True)
    #plot_smallr_toy(combs_rxTrue_toy[3], combs_rxTrue_data[3], 'comb3' , prefix_True)
    #plot_smallr_toy(combs_rxTrue_toy[4], combs_rxTrue_data[4], 'comb4' , prefix_True)
    #plot_smallr_toy(combs_rxTrue_toy[5], combs_rxTrue_data[5], 'comb5' , prefix_True)
    #plot_smallr_toy(combs_rxTrue_toy[6], combs_rxTrue_data[6], 'comb6' , prefix_True)

    ##plot toy resutls for wcs
    #plot_WCs_toys(combs_rxFalse_toy[0], combs_rxTrue_toy[0], combs_rxFalse_data[0], combs_rxTrue_data[0], 'combSM', prefix_True)
    #plot_WCs_toys(combs_rxFalse_toy[1], combs_rxTrue_toy[1], combs_rxFalse_data[1], combs_rxTrue_data[1], 'comb1' , prefix_True)
    #plot_WCs_toys(combs_rxFalse_toy[2], combs_rxTrue_toy[2], combs_rxFalse_data[2], combs_rxTrue_data[2], 'comb2' , prefix_True)
    #plot_WCs_toys(combs_rxFalse_toy[3], combs_rxTrue_toy[3], combs_rxFalse_data[3], combs_rxTrue_data[3], 'comb3' , prefix_True)
    #plot_WCs_toys(combs_rxFalse_toy[4], combs_rxTrue_toy[4], combs_rxFalse_data[4], combs_rxTrue_data[4], 'comb4' , prefix_True)
    #plot_WCs_toys(combs_rxFalse_toy[5], combs_rxTrue_toy[5], combs_rxFalse_data[5], combs_rxTrue_data[5], 'comb5' , prefix_True)
    #plot_WCs_toys(combs_rxFalse_toy[6], combs_rxTrue_toy[6], combs_rxFalse_data[6], combs_rxTrue_data[6], 'comb6' , prefix_True)

    #look at the frequency of each of the combinations
    #for i in range(6):
    #    cond_False = toy_res[0] == delta_chisq_False_toy[:, i]
    #    print(i, 'False', delta_chisq_False_toy[cond_False].shape[0])
    #    cond_True = toy_res[1] == delta_chisq_True_toy[:, i]
    #    print(i, 'True', delta_chisq_True_toy[cond_True].shape[0])
    ###########

    return toy_res, data_res, fnames

def main():
    ##generate fake data
    #np.random.seed(0)
    #toy_res  = (chi2(5, loc=0, scale=1).rvs(size=50000), chi2(3, loc=0, scale=1).rvs(size=50000))
    #data_res = (30., 30.)
    ###print(data)
    ###plt.hist(data, bins=50)
    ###plt.show()

    ##get actual data
    #prefix_True    = 'RX'; chisq_range = (0., 55.)
    #prefix_True    = 'RXFixAllr'; chisq_range = (0., 55.)
    #prefix_True    = 'RpKLHCb';  chisq_range = (0., 40.)
    prefix_True    = 'RXWideRange'; chisq_range = (0., 55.)
    #prefix_True   = 'LFU_RpKLHCb';  chisq_range = (0., 25.)

    #prefix_True    = 'RXReducedUncertaintyr7pK'; chisq_range = (0., 55.)
    #prefix_True    = 'RXFixr7';   chisq_range = (0., 55.)
    #prefix_True   = 'LFU_RpKLHCbReducedUncertainty';  chisq_range = (0., 25.)
    #prefix_True   = 'LFU_RpK';  chisq_range = (0., 25.)
    #prefix_True   = 'LFU_RpKReducedUncertainty';  chisq_range = (0., 25.)
    #prefix_True   = 'LFU_RpKLHCbFixSmallr';  chisq_range = (0., 25.)

    toy_res , data_res, fnames = get_fitresults(prefix_True)

    #fit and plot
    ires = 0
    fit_results = []
    for toys, data, fname in zip(toy_res, data_res, fnames):
        #set floated params
        fit_parameters = {}
        fit_parameters['ddof']  = FitParameter('ddof'  , 5.0, 0  , 10, 0.01)
        if 'LFU' in prefix_True:
            fit_parameters['loc']   = FitParameter('loc'   , 0.0, -10, 10, 0.1)
            fit_parameters['scale'] = FitParameter('scale' , 1.0, 0  , 10, 0.1)
        else:
            fit_parameters['loc']   = FitParameter('loc'   , 0.0, -10, 10, 0.1)
            fit_parameters['scale'] = FitParameter('scale' , 1.0, 0  , 10, 0.1)

        ##do the minimisation
        #params = {fp: fit_parameters[fp].value for fp in fit_parameters}
        #print(cost_func_chi2(params, toys))
        #exit(1)

        ##res = chi2.fit(toys, method="MM")
        #res = chi2.fit(toys, floc=0, fscale=1.)
        #results = {}
        #results['params'] = {}
        #results['params']['ddof'] = (res[0],)
        #results['params']['loc'] = (res[1],)
        #results['params']['scale'] = (res[2],)

        results = Minimize(fit_parameters, cost_func_chi2, args=(toys,), methods=['MIGRAD'], nfits = 1, use_hesse = False, use_minos = False, get_covariance = False, randomise = False, disp = True)
        results['params']['loc'] = (fit_parameters['loc'].value,)
        results['params']['scale'] = (fit_parameters['scale'].value,)
        print(results)
        fit_results += [results]

        ##plot the distribution
        #fit_dof = len(list(fit_parameters.values()))
        #if ires == 0: 
        #    plot_chisq(toys, results, data, fname = fname, fit_dof = fit_dof, plot_davide = False)
        #else:
        #    plot_chisq(toys, results, data, x_range = (0., 55.), fname = fname, fit_dof = fit_dof)

        ires += 1

    plot_chisq_both(toy_res[0], fit_results[0], data_res[0], toy_res[1], fit_results[1], data_res[1], x_range = chisq_range, fname = 'plots/chisq_both.pdf', prefix = prefix_True)

if __name__ == '__main__':
    start = time.time()

    main()

    end = time.time()
    print('Time taken in min', (end - start)/60., 'after fitting is complete')
