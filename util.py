#flavio
import flavio
from wilson import Wilson
from flavio.classes import Measurement
from flavio.math.functions import round_to_sig_dig
from flavio.statistics.probability import DeltaDistribution, MultivariateNormalDistribution, NormalDistribution, ProbabilityDistribution
from flavio.statistics.likelihood import Likelihood
from flavio.classes import Observable, Prediction, Parameter
from flavio.physics.bdecays.wilsoncoefficients import wctot_dict
from scipy.optimize import minimize
from scipy.stats import truncnorm
from optimparallel import minimize_parallel
#from flavio.physics import ckm
#from flavio.physics.running import running
#from flavio.physics.bdecays.common import meson_quark, lambda_K
#from flavio.config import config
#from flavio.physics.bdecays.wilsoncoefficients import wctot_dict

#other libraries
import numpy as np
from timeit import default_timer as timer
from iminuit import Minuit
import iminuit

small_rparams = []
small_rparams += ['rL_pK']     
small_rparams += ['rL_Kpi']    
small_rparams += ['rL_Kpipi']  
small_rparams += ['r7_pK']    
small_rparams += ['r7_Kpi']   
small_rparams += ['r7_Kpipi'] 
small_rparams += ['r1_pK']    
small_rparams += ['r2_pK']    
small_rparams += ['r1_Kpi']   
small_rparams += ['r2_Kpi']   
small_rparams += ['r1_Kpipi'] 
small_rparams += ['r2_Kpipi'] 

class TruncatedNormalDistribution(ProbabilityDistribution):
    def __init__(self, central_value, standard_deviation, lower_limit, upper_limit):
        super().__init__(central_value,
                         support=(lower_limit,
                                  upper_limit))
        if standard_deviation <= 0:
            raise ValueError("Standard deviation must be positive number")
        self.standard_deviation = standard_deviation
        self.lower_limit = lower_limit
        self.upper_limit = upper_limit
        self.a = (self.lower_limit - self.central_value) / self.standard_deviation
        self.b = (self.upper_limit - self.central_value) / self.standard_deviation
        self.distb =  truncnorm(self.a, self.b, loc=self.central_value, scale = self.standard_deviation)

    def __repr__(self):
        return 'flavio.statistics.probability.TruncatedNormalDistribution' + \
               '({}, {}, {}, {})'.format(self.central_value, self.standard_deviation, self.lower_limit, self.upper_limit)

    def get_random(self, size=None):
        return self.distb.rvs(size)

    def logpdf(self, x):
        return np.log(self.pdf(x))

    def pdf(self, x):
        #need to do this since log value will be inf leading to problems in minimisation
        pdf  = self.distb.pdf(x)
        cond = np.equal(np.zeros_like(pdf), pdf)
        return np.where(cond, 1e-20 * np.ones_like(pdf), pdf)

    def cdf(self, x):
        return self.distb.cdf(x)

    def ppf(self, x):
        return self.distb.ppf(x)

    def get_error_left(self, nsigma=1, **kwargs):
        left_limit = nsigma * self.standard_deviation
        if left_limit < self.lower_limit:
            return self.lower_limit
        else:
            return left_limit

    def get_error_right(self, nsigma=1, **kwargs):
        right_limit = nsigma * self.standard_deviation
        if right_limit > self.upper_limit:
            return self.upper_limit
        else:
            return right_limit

class FitParameter:
    def __init__(self, name, init_value, lower_limit, upper_limit, step_size=1e-6):
        self.name  = name
        self.value = init_value
        self.init_value = init_value
        self.lower_limit = lower_limit
        self.upper_limit = upper_limit
        self.step_size = step_size
        self.prev_value = None
        self.fixed = False
        self.error = 0.0
        self.positive_error = 0.0
        self.negative_error = 0.0
        self.fitted_value = init_value

    def update(self, value):
        if value != self.prev_value:
            self.value = value
            self.prev_value = value

    def __call__(self):
        return self.value

    def fix(self):
        self.fixed = True

    def float(self):
        self.fixed = False

    def setFixed(self, fixed):
        self.fixed = fixed

    def floating(self):
        return self.step_size > 0 and not self.fixed

def get_kstmumu_cov_mat(obs_names, q2min, q2max):
    """Define a function to get the mean and covariance matrix of kstmumu observables in a given q2 bin"""
    ms_name_pbase = 'LHCb B->K*mumu 2020 P {0:.3g}-{1:.3g}'.format(q2min,q2max)
    m_obj_pbase   = Measurement.instances[ms_name_pbase]
    cov_flavio_pbase = m_obj_pbase._parameters[(obs_names[0], q2min, q2max)][1].get_cov_mat()

    if '<S5>(B0->K*mumu)' in obs_names:
        ms_name_sbase = 'LHCb B->K*mumu 2020 S {0:.3g}-{1:.3g}'.format(q2min,q2max)
        m_obj_sbase   = Measurement.instances[ms_name_sbase]
        cov_flavio_sbase = m_obj_sbase._parameters[(obs_names[0], q2min, q2max)][1].get_cov_mat()
        cov_fL_s5 = cov_flavio_sbase[0][3]
        cov_s5    = cov_flavio_sbase[3][3]
        cov_flavio_pbase[5,:] = np.array([cov_fL_s5] + 4*[0.0] + [cov_s5] + 2*[0.])
        cov_flavio_pbase[:,5] = np.array([cov_fL_s5] + 4*[0.0] + [cov_s5] + 2*[0.])

    return cov_flavio_pbase

def get_obs_properties(name, toy_index, use_SM_pred, rpKerr_scaledown_run12 = True):
    #define measurement name
    measurement_name = name+'_toy_'+str(toy_index)
    experiment_name  = 'LHCb'
    if name == 'RK': #Full Run1-2 data
        #define observable names
        obs_list       = ['<Rmue>(B+->Kll)']
        scale_err_list = [0.05] #(used by Davide in project1, could be 0.052)
        #define q2 bins
        q2_bins  = []
        q2_bins += [(1.1  , 6.0)]
    elif name == 'RKst': #Run1 data
        #define observable names
        obs_list       = ['<Rmue>(B0->K*ll)']
        scale_err_list = [0.17] #low bin gives 17.3% and high bin gives 17.5 (17% used by Davide)
        #define q2 bins
        q2_bins  = []
        q2_bins += [(0.045, 1.1)]
        q2_bins += [(1.1  , 6.0)]
    elif name == 'BsTomumu':
        #define observable names
        obs_list       = ["BR(Bs->mumu)"]
        scale_err_list = [0.10] #Gino's combination used by Davide
        #define q2 bins
        q2_bins  = None
    elif name == 'RpK': #NB: This measurement was done with Run1, the Run1-2 projection gives an uncertainty of 11.9%
        #define observable names
        obs_list       = ['<RpK>(Lb->pKll)']
        scale_err_list = [0.173] 
        if rpKerr_scaledown_run12: scale_err_list = [1/np.sqrt(2) * scale_err_list[0]] #uncertainty scaled down by a factor 1/sqrt(2) to include both Run1-2, but why only a factor two?
        #define q2 bins
        q2_bins  = []
        q2_bins += [(0.1  , 6.0)]
    elif name == 'RKS': #NB: This measurement was done with Run1, the Run1-2 projection gives an uncertainty of 11.9%
        #define observable names
        obs_list       = ['<Rmue>(B0->Kll)']
        scale_err_list = [0.31] 
        #define q2 bins
        q2_bins  = []
        q2_bins += [(1.1  , 6.0)]
    elif name == 'RKstplus': #NB: This measurement was done with Run1, the Run1-2 projection gives an uncertainty of 11.9%
        #define observable names
        obs_list       = ['<Rmue>(B+->K*ll)']
        scale_err_list = [0.26] 
        #define q2 bins
        q2_bins  = []
        q2_bins += [(0.045  , 6.0)]
    elif name == 'RKpi': #NB: Projection for Run1-2 data
        #define observable names
        obs_list       = ['<RKpi>(B0->Kpill)']
        scale_err_list = [0.077] #from Rafeal
        #define q2 bins
        q2_bins  = []
        q2_bins += [(1.1  , 6.0)] #The analysis will however be done to 7 GeV^2 (for RX paper we have only the 6 GeV of RK* as the public info)
    elif name == 'RKpipi':#NB: Projection for Run1-2 data
        #define observable names
        obs_list       = ['<RKpipi>(B+->Kpipill)']
        scale_err_list = [0.135] #from Rafeal
        #define q2 bins
        q2_bins  = []
        q2_bins += [(1.1  , 6.0)] #The analysis will however be done to 7 GeV^2 (for RX paper we have only the 6 GeV of RK* as the public info)
    elif name == 'B0ToKstmumu': #Full Run1 and Run2 data
        #define observable names
        obs_list        = []
        obs_list       += ['<FL>(B0->K*mumu)' ]
        obs_list       += ['<P1>(B0->K*mumu)' ]
        obs_list       += ['<P2>(B0->K*mumu)' ]
        obs_list       += ['<P3>(B0->K*mumu)' ]
        obs_list       += ['<P4p>(B0->K*mumu)']
        #NB!!!!: It HAS to be 5th entry, so do not change the position!! (the position is used by get_kstmumu_cov_mat to make new covariance matrix when S5 is involved)
        if use_SM_pred:
            obs_list       += ['<P5p>(B0->K*mumu)']
        else:
            obs_list       += ['<S5>(B0->K*mumu)']

        obs_list       += ['<P6p>(B0->K*mumu)']
        obs_list       += ['<P8p>(B0->K*mumu)']
        #define q2 bins
        q2_bins  = []
        q2_bins += [(0.10,0.98)]
        q2_bins += [(1.10,2.50)]
        q2_bins += [(2.50,4.00)]
        q2_bins += [(4.00,6.00)]
        q2_bins += [(6.00,8.00)]
        q2_bins += [(11.0,12.5)]
        q2_bins += [(15.0,17.0)]
        q2_bins += [(17.0,19.0)]
        #define cov matrix for each q2 bin
        obs_cov_matrices = [get_kstmumu_cov_mat(obs_list, q2_b[0], q2_b[1]) for q2_b in q2_bins]
        scale_err_list   = obs_cov_matrices
    else:
        raise Exception('The passed name', name, 'not implemented!')

    return (measurement_name, experiment_name, obs_list, scale_err_list, q2_bins)

def get_kstmumu_cent_val(obs_name, q2_limits):
    q2min = q2_limits[0]
    q2max = q2_limits[1]
    #p-base
    ms_name_pbase = 'LHCb B->K*mumu 2020 P {0:.3g}-{1:.3g}'.format(q2min,q2max)
    m_obj_pbase   = Measurement.instances[ms_name_pbase]
    central_pbase = m_obj_pbase.get_central_all()
    #s-base
    ms_name_sbase = 'LHCb B->K*mumu 2020 S {0:.3g}-{1:.3g}'.format(q2min,q2max)
    m_obj_sbase   = Measurement.instances[ms_name_sbase]
    central_sbase = m_obj_sbase.get_central_all()
    #get central value
    c_key = (obs_name, q2min, q2max)
    val   = None
    if obs_name == '<S5>(B0->K*mumu)':
        val = central_sbase[c_key]
    else:
        val = central_pbase[c_key]

    return val

def add_measurement_correlated_obs(measurement_name, experiment_name, obs_list, obs_cov_mat_list, q2_bins = None, use_SM_pred = True, smear = True):
    """Add a measurement given name and observable lists that are correlated"""
    m=Measurement(measurement_name)
    m.experiment=experiment_name
    all_obs_list=[]
    if q2_bins is not None:
        for n, q2_b in enumerate(q2_bins):
            obs_vals=[flavio.sm_prediction(obs_name, q2min=q2_b[0], q2max=q2_b[1]) for obs_name in obs_list]
            if not use_SM_pred:
                obs_vals= [get_kstmumu_cent_val(obs_name, q2_limits = (q2_b[0], q2_b[1])) for obs_name in obs_list]

            if smear:
                _obs_vals  =np.random.multivariate_normal(obs_vals,obs_cov_mat_list[n])
                obs_vals   =[round_to_sig_dig(_obs_vals[i], sig_dig=3) for i in range(len(_obs_vals))]

            multivariate_dist = MultivariateNormalDistribution(obs_vals,covariance=obs_cov_mat_list[n])
            obs_lt = [(obs_name, q2_b[0] , q2_b[1]) for obs_name in obs_list]
            m.add_constraint(obs_lt, multivariate_dist)        
            all_obs_list += obs_lt
    else:
        #obs_vals=[flavio.sm_prediction(obs_name) for obs_name in obs_list]
        #if not use_SM_pred:
        #    obs_vals=[get_exp_measurement(obs_name) for obs_name in obs_list]

        #if smear:
        #    _obs_vals=np.random.multivariate_normal(obs_vals,obs_cov_mat_list)
        #    obs_vals =[round_to_sig_dig(_obs_vals[i], sig_dig=3) for i in range(len(_obs_vals))]

        #multivariate_dist = MultivariateNormalDistribution(obs_vals,covariance=obs_cov_mat_list)
        #obs_lt = [(obs_name) for obs_name in obs_list]
        #m.add_constraint(obs_lt, multivariate_dist)        
        #all_obs_list += obs_lt
        raise Exception('Non q2 correlation observalbes not yet implemented!')

    return all_obs_list

def get_data_distribution(measurement_key, rpK_lhcb_central_val = False, rpKerr_scaledown_run12 = True):
    norm_dist = None
    if isinstance(measurement_key, str):
        if measurement_key == 'BR(Bs->mumu)':
            mu  = 2.83e-9
            sig = 0.32e-9
            norm_dist=NormalDistribution(mu,sig)
        else:
            raise Exception('The measurement_key {0} is not implemented. Please check!'.format(measurement_key))
    elif isinstance(measurement_key, tuple) and len(measurement_key) == 3:
        if isinstance(measurement_key[0], str) and isinstance(measurement_key[1], float) and isinstance(measurement_key[2], float):
            cond = measurement_key[0] == '<Rmue>(B+->Kll)' or measurement_key[0] == '<Rmue>(B0->K*ll)' 
            cond = cond or measurement_key[0] == '<Rmue>(B0->Kll)' or measurement_key[0] == '<Rmue>(B+->K*ll)' 
            if cond:
                ms_name = 'LHCb RK 2021'
                if measurement_key[0] == '<Rmue>(B0->K*ll)':
                    ms_name = 'LHCb RK* 2017 with systematic'
                elif measurement_key[0] == '<Rmue>(B0->Kll)':
                    ms_name = 'LHCb RKS 2021'
                elif measurement_key[0] == '<Rmue>(B+->K*ll)':
                    ms_name = 'LHCb RKstplus 2021'

                m_obj   = Measurement.instances[ms_name]
                if measurement_key in m_obj.get_central_all():
                    norm_dist = m_obj._parameters[measurement_key][1]
                else:
                    raise Exception('The observable {0} does not exist in this flavio measurement {1}. Please check!'.format(measurement_key, ms_name))

            elif measurement_key[0] == '<RpK>(Lb->pKll)':
                mu  = 0.8
                if rpK_lhcb_central_val: mu  = 0.86

                sig = 0.173 * mu
                if rpKerr_scaledown_run12: sig = 1/np.sqrt(2) * sig

                norm_dist=NormalDistribution(mu,sig)
            elif measurement_key[0] == '<RKpi>(B0->Kpill)':
                mu  = 0.8
                sig = 0.077 * mu
                norm_dist=NormalDistribution(mu,sig)
            elif measurement_key[0] == '<RKpipi>(B+->Kpipill)':
                mu  = 0.8
                sig = 0.135 * mu
                norm_dist=NormalDistribution(mu,sig)
            else:
                raise Exception('The measurement_key {0} not implemented yet!')
        else:
            raise Exception('The measurement_key contains 3 entries but are not of type (str,float,float). Please check!')
    else:
        raise Exception('The measurement_key is neither a string nor tuple of size 3. Please check!')
    
    return norm_dist

def add_measurement_uncorrelated_obs(measurement_name, experiment_name, obs_list, scale_err_list, q2_bins = None, use_SM_pred = True, smear = True, rpK_lhcb_central_val = False, rpKerr_scaledown_run12 = True):
    """Add a measurement given name and observable lists that are uncorrelated"""
    m=Measurement(measurement_name)
    m.experiment=experiment_name
    all_obs_list=[]
    for obs_name, obs_scale_err in zip(obs_list, scale_err_list):
        if q2_bins is not None:
            for q2_b in q2_bins:
                measurement_key = (obs_name, q2_b[0], q2_b[1])
                if use_SM_pred:
                    obs_vals=flavio.sm_prediction(measurement_key[0], q2min=measurement_key[1], q2max=measurement_key[2])
                    obs_err = obs_scale_err * obs_vals
                    #print('SM value for ', obs_name, obs_vals, obs_scale_err, obs_scale_err * obs_vals)
                    if smear:
                        obs_vals=np.random.normal(loc=obs_vals, scale=obs_err, size=(1))[0]

                    #print('SM smeared value for ', obs_name, obs_vals)
                    mu  = round_to_sig_dig(obs_vals  ,3)
                    sig = round_to_sig_dig(obs_err,3)
                    norm_dist=NormalDistribution(mu,sig)
                else:
                    if smear:
                        raise Exception('This feature is not implemented!')

                    #get from flavio the measurement distribution directly
                    norm_dist=get_data_distribution(measurement_key, rpK_lhcb_central_val = rpK_lhcb_central_val, rpKerr_scaledown_run12 = rpKerr_scaledown_run12)

                m.add_constraint([measurement_key], norm_dist)        
                all_obs_list.append(measurement_key)
        else:
            measurement_key = (obs_name)
            if use_SM_pred:
                obs_vals=flavio.sm_prediction(measurement_key)
                obs_err = obs_scale_err * obs_vals
                #print('SM value for ', obs_name, obs_vals, obs_scale_err, obs_scale_err * obs_vals)
                if smear:
                    obs_vals=np.random.normal(loc=obs_vals, scale=obs_err, size=(1))[0]

                mu  = round_to_sig_dig(obs_vals  ,3)
                sig = round_to_sig_dig(obs_err,3)
                #print('SM smeared value for ', obs_vals[0], round_to_sig_dig(obs_vals[0]  ,3))
                norm_dist=NormalDistribution(mu,sig)
            else:
                if smear:
                    raise Exception('This feature is not implemented!')

                #get from flavio the measurement distribution directly
                norm_dist = get_data_distribution(measurement_key, rpK_lhcb_central_val = rpK_lhcb_central_val, rpKerr_scaledown_run12 = rpKerr_scaledown_run12)

            m.add_constraint([measurement_key], norm_dist)        
            all_obs_list.append(measurement_key)

    return all_obs_list

def check_measurement(measurement_name, q2_bins = None):
    """Check all the observables in a measurement given the names"""
    m_ob         = Measurement.instances[measurement_name]
    obsname_list = m_ob.all_parameters
    obs_cen      = m_ob.get_central_all()
    obs_err      = m_ob.get_1d_errors_rightleft()
    print('In measurement', measurement_name, 'we have following values for the observables')
    for obsn in obsname_list:
        if q2_bins is not None:
            if obs_cen[obsn] > 1e4 or obs_cen[obsn] < 1e-4:
                print('Obsv: {0}, q2 bin: ({1}, {2}), value: {3:1.3e} +/- ^({4:1.3e})_({5:1.3e})'.format(obsn[0], obsn[1], obsn[2], obs_cen[obsn], obs_err[obsn][0], obs_err[obsn][1]))
            else:
                print('Obsv: {0}, q2 bin: ({1}, {2}), value: {3:1.3f} +/- ^({4:1.3f})_({5:1.3f})'.format(obsn[0], obsn[1], obsn[2], obs_cen[obsn], obs_err[obsn][0], obs_err[obsn][1]))
        else:
            if obs_cen[obsn] > 1e4 or obs_cen[obsn] < 1e-4:
                print('Obsv: {0}, value: {1:.5e} +/- ^({2:.5e})_({3:.5e})'.format(obsn, obs_cen[obsn], obs_err[obsn][0], obs_err[obsn][1]))
            else:
                print('Obsv: {0}, value: {1:1.3f} +/- ^({2:1.3f})_({3:1.3f})'.format(obsn, obs_cen[obsn], obs_err[obsn][0], obs_err[obsn][1]))

def rx_gino_func(obs_name):
    rx_type = None
    if obs_name == '<RpK>(Lb->pKll)':
        rx_type = 'pK'
    elif obs_name == '<RKpi>(B0->Kpill)':
        rx_type = 'Kpi'
    elif obs_name == '<RKpipi>(B+->Kpipill)':
        rx_type = 'Kpipi'
    else:
        raise Exception('The observation name not recognised!')

    def inner_rx_gino_func(wc, par, q2min, q2max):
        #define a Gamma functions
        dGdq2_intgl_mu_func = lambda a: (C9_mu - a * C10_mu)**2 + (C9p_mu - a * C10p_mu)**2 + rL * (C9_mu - a * C10_mu) * (C9p_mu - a * C10p_mu) + C7 * (r7 * C7 + r1 * (C9_mu - a * C10_mu) + r2 * (C9p_mu - a * C10p_mu))
        dGdq2_intgl_e_func  = lambda a: (C9_e  - a * C10_e )**2 + (C9p_e  - a * C10p_e )**2 + rL * (C9_e  - a * C10_e ) * (C9p_e  - a * C10p_e ) + C7 * (r7 * C7 + r1 * (C9_e  - a * C10_e ) + r2 * (C9p_e  - a * C10p_e))
        #get wc
        wc_mu  = wctot_dict(wc_obj=wc, sector='bsmumu', scale=4.8, par=par, nf_out=5)
        wc_e   = wctot_dict(wc_obj=wc, sector='bsee'  , scale=4.8, par=par, nf_out=5)
        #universal C7: ##flavour dependent #get r_i
        r7     = par['r7_'+rx_type]
        r1     = par['r1_'+rx_type]
        r2     = par['r2_'+rx_type]
        rL     = par['rL_'+rx_type]
        #r1     = 0.
        #r2     = 0.
        #rL     = 2.
        C7     = wc_mu['C7eff_bs']      
        C9_mu  = wc_mu['C9_bsmumu']
        C9p_mu = wc_mu['C9p_bsmumu']
        C10_mu = wc_mu['C10_bsmumu']
        C10p_mu= wc_mu['C10p_bsmumu']
        C9_e   =  wc_e['C9_bsee']
        C9p_e  =  wc_e['C9p_bsee']
        C10_e  =  wc_e['C10_bsee']
        C10p_e =  wc_e['C10p_bsee']
        #rL       = 4.0
        #C9_mu    = 2.0
        #C9p_mu   = 1.0
        #C10_mu   = 1.5
        #C10p_mu  = 0.5
        #C9_e     = 3.0
        #C9p_e    = 0.0
        #C10_e    = 2.0
        #C10p_e   = 0.0
        q2_binwidth    = (q2max - q2min)
        dGdq2_intgl_mu = (dGdq2_intgl_mu_func(1.) + dGdq2_intgl_mu_func(-1.) )/q2_binwidth #Units of GeV^2
        dGdq2_intgl_e  = ( dGdq2_intgl_e_func(1.) +  dGdq2_intgl_e_func(-1.) )/q2_binwidth #Units of GeV^2
        r_x = dGdq2_intgl_mu/dGdq2_intgl_e
        #print(rx_type)
        #print('rL', rL, 'r7', r7, 'r1', r1, 'r2', r2)
        #print('C9mu', C9_mu, 'C9pmu', C9p_mu, 'C10mu', C10_mu, 'C10pmu', C10p_mu)
        #print('C9e', C9_e, 'C9pe', C9p_e, 'C10e', C10_e, 'C10pe', C10p_e)
        #print('C7' , C7)
        #print('r_x' ,  r_x)
        return r_x

    return inner_rx_gino_func

def make_newparameters(fit_parameters, floated_small_rs):
    #if not small r floated return None
    if len(floated_small_rs) == 0: 
        return None

    #Make any new parameters needed and overload the default set
    for fsmr in floated_small_rs: 
        _  = Parameter(fsmr) 
        #Add DeltaDistribution as constraint
        #flavio.parameters.read_file_values('/home/hep/amathad/Packages/flavio/flavio/data/myparameter.yml', flavio.default_parameters)
        flavio.default_parameters.set_constraint(fsmr, constraint_string=str(fit_parameters[fsmr].value))

def make_newobservables(obsn):
    obs = Observable(obsn, arguments=['q2min', 'q2max'])
    _   = Prediction(obs.name, rx_gino_func(obs.name))

def add_constraints(constrained_fit_params):
    #set the constraints on the small r's
    for cfp in list(constrained_fit_params.keys()):
        print('Adding constraints on {0} with {1}'.format(cfp, constrained_fit_params[cfp]))
        flavio.default_parameters.add_constraint([cfp]   , constrained_fit_params[cfp])

def add_observables(name, toy_index, use_SM_pred = True, smear = True, rpK_lhcb_central_val = False, rpKerr_scaledown_run12 = True):
    #get the properties of the observables given the name. The name should be one already predefined
    measurement_name, experiment_name, obs_list, scale_err_list, q2_bins = get_obs_properties(name, toy_index, use_SM_pred=use_SM_pred, rpKerr_scaledown_run12 = rpKerr_scaledown_run12)

    #add a measurement and all observables 
    #observables are added with central value that is "SM-smeared prediction" (according to experimental measurement) 
    #also make a list of all observables defined (used in construction of likelihood)
    if name == 'B0ToKstmumu':        
        #NB: scale_err_list is actually now the covariance matrix in a given q2 and costheta mu bin
        all_obs_list =  add_measurement_correlated_obs(measurement_name, experiment_name, obs_list, scale_err_list, q2_bins = q2_bins, use_SM_pred = use_SM_pred, smear = smear)
    else:
        all_obs_list =  add_measurement_uncorrelated_obs(measurement_name, experiment_name, obs_list, scale_err_list, q2_bins = q2_bins, use_SM_pred = use_SM_pred, smear = smear, rpK_lhcb_central_val = rpK_lhcb_central_val, rpKerr_scaledown_run12 = rpKerr_scaledown_run12)

    #check that the observables are added correctly 
    check_measurement(measurement_name, q2_bins=q2_bins)

    #return a dictionary of measurement name and all observable names
    return {measurement_name: all_obs_list}

def cost_func_rx(param_dict, *args):
    likelihood = args[0][0]
    floated_small_rs = args[0][1]

    #Not sensitive to the sum of CS and CSP, assume zero
    sum_CS_CSP=0.

    #determine the combination type
    combination_type='combSM'
    if 'deltaC10' in param_dict.keys() and 'deltaC9' in param_dict.keys():
        combination_type = 'comb1'
    elif 'deltaC9' in param_dict.keys() and 'deltaC9p' in param_dict.keys():
        combination_type = 'comb2'
    elif 'deltaC9' in param_dict.keys() and 'deltaC10p' in param_dict.keys():
        combination_type = 'comb3'
    elif 'deltaC10' in param_dict.keys() and 'deltaC10p' in param_dict.keys():
        combination_type = 'comb4'
    elif 'deltaC9p' in param_dict.keys() and 'deltaC10p' in param_dict.keys():
        combination_type = 'comb5' 
    elif 'deltaC9p' in param_dict.keys() and 'deltaC10' in param_dict.keys():
        combination_type = 'comb6'                    

    #small r_is 
    flavio_default_params = flavio.parameters.default_parameters.get_central_all() 
    for srfp in floated_small_rs: flavio_default_params[srfp] = param_dict[srfp] 

    #define wc_dict
    wc_dict    = {}
    wc_dict['C9_bsmumu'  ]            = param_dict['C9mu' ]
    if combination_type == 'comb1':
        wc_dict['C10_bsmumu' ]            = param_dict['C10mu']
        wc_dict['C9p_bsmumu' ]            = param_dict['C9pmu'] 
        wc_dict['C10p_bsmumu']            = param_dict['C10pmu']
        wc_dict['CS_bsmumu'  ]            = 0.5 * (sum_CS_CSP + param_dict['delta_CS_CSp'])
        wc_dict['CSp_bsmumu' ]            = 0.5 * (sum_CS_CSP - param_dict['delta_CS_CSp'])
        wc_dict['C9_bsee'    ]            = (param_dict['C9mu']   - param_dict['deltaC9']  )
        wc_dict['C10_bsee'   ]            = (param_dict['C10mu']  - param_dict['deltaC10'] )
    elif combination_type == 'comb2':
        wc_dict['C10_bsmumu' ]            = param_dict['C10mu']
        wc_dict['C9p_bsmumu' ]            = param_dict['C9pmu'] 
        wc_dict['C10p_bsmumu']            = param_dict['C10pmu']
        wc_dict['CS_bsmumu'  ]            = 0.5 * (sum_CS_CSP + param_dict['delta_CS_CSp'])
        wc_dict['CSp_bsmumu' ]            = 0.5 * (sum_CS_CSP - param_dict['delta_CS_CSp'])
        wc_dict['C9_bsee'    ]            = (param_dict['C9mu']   - param_dict['deltaC9']  )
        wc_dict['C9p_bsee'   ]            = (param_dict['C9pmu']  - param_dict['deltaC9p'] )
    elif combination_type == 'comb3':
        wc_dict['C10_bsmumu' ]            = param_dict['C10mu']
        wc_dict['C9p_bsmumu' ]            = param_dict['C9pmu'] 
        wc_dict['C10p_bsmumu']            = param_dict['C10pmu']
        wc_dict['CS_bsmumu'  ]            = 0.5 * (sum_CS_CSP + param_dict['delta_CS_CSp'])
        wc_dict['CSp_bsmumu' ]            = 0.5 * (sum_CS_CSP - param_dict['delta_CS_CSp'])
        wc_dict['C9_bsee'    ]            = (param_dict['C9mu']   - param_dict['deltaC9']  )
        wc_dict['C10p_bsee'  ]            = (param_dict['C10pmu'] - param_dict['deltaC10p'])
    elif combination_type == 'comb4':
        wc_dict['C10_bsmumu' ]            = param_dict['C10mu']
        wc_dict['C9p_bsmumu' ]            = param_dict['C9pmu'] 
        wc_dict['C10p_bsmumu']            = param_dict['C10pmu']
        wc_dict['CS_bsmumu'  ]            = 0.5 * (sum_CS_CSP + param_dict['delta_CS_CSp'])
        wc_dict['CSp_bsmumu' ]            = 0.5 * (sum_CS_CSP - param_dict['delta_CS_CSp'])
        wc_dict['C10_bsee'   ]            = (param_dict['C10mu']  - param_dict['deltaC10'] )
        wc_dict['C10p_bsee'  ]            = (param_dict['C10pmu'] - param_dict['deltaC10p'])
    elif combination_type == 'comb5':
        wc_dict['C10_bsmumu' ]            = param_dict['C10mu']
        wc_dict['C9p_bsmumu' ]            = param_dict['C9pmu'] 
        wc_dict['C10p_bsmumu']            = param_dict['C10pmu']
        wc_dict['CS_bsmumu'  ]            = 0.5 * (sum_CS_CSP + param_dict['delta_CS_CSp'])
        wc_dict['CSp_bsmumu' ]            = 0.5 * (sum_CS_CSP - param_dict['delta_CS_CSp'])
        wc_dict['C9p_bsee'   ]            = (param_dict['C9pmu']  - param_dict['deltaC9p'] )
        wc_dict['C10p_bsee'  ]            = (param_dict['C10pmu'] - param_dict['deltaC10p'])
    elif combination_type == 'comb6':
        wc_dict['C10_bsmumu' ]            = param_dict['C10mu']
        wc_dict['C9p_bsmumu' ]            = param_dict['C9pmu'] 
        wc_dict['C10p_bsmumu']            = param_dict['C10pmu']
        wc_dict['CS_bsmumu'  ]            = 0.5 * (sum_CS_CSP + param_dict['delta_CS_CSp'])
        wc_dict['CSp_bsmumu' ]            = 0.5 * (sum_CS_CSP - param_dict['delta_CS_CSp'])
        wc_dict['C9p_bsee'   ]            = (param_dict['C9pmu']  - param_dict['deltaC9p'] )
        wc_dict['C10_bsee'   ]            = (param_dict['C10mu']  - param_dict['deltaC10'] )
    elif combination_type == 'combSM': #Only C9Universal floated
        wc_dict['C9_bsee'    ]            = wc_dict['C9_bsmumu'  ]            

    ##print the values
    #for k in list(wc_dict.keys()): print(k, wc_dict[k])
    wilson = Wilson(wc_dict, scale=4.8, eft='WET', basis='flavio')
    return -2. * likelihood.log_likelihood(flavio_default_params, wilson) 

def cost_func_rx_lfuv(param_dict, *args):
    likelihood = args[0][0]
    floated_small_rs = args[0][1]

    #determine the combination type
    combination_type='combSM'
    if 'deltaC10' in param_dict.keys() and 'deltaC9' in param_dict.keys():
        combination_type = 'comb1'
    elif 'deltaC9' in param_dict.keys() and 'deltaC9p' in param_dict.keys():
        combination_type = 'comb2'
    elif 'deltaC9' in param_dict.keys() and 'deltaC10p' in param_dict.keys():
        combination_type = 'comb3'
    elif 'deltaC10' in param_dict.keys() and 'deltaC10p' in param_dict.keys():
        combination_type = 'comb4'
    elif 'deltaC9p' in param_dict.keys() and 'deltaC10p' in param_dict.keys():
        combination_type = 'comb5' 
    elif 'deltaC9p' in param_dict.keys() and 'deltaC10' in param_dict.keys():
        combination_type = 'comb6'                    

    #small r_is 
    flavio_default_params = flavio.parameters.default_parameters.get_central_all() 
    for srfp in floated_small_rs: flavio_default_params[srfp] = param_dict[srfp] 

    #define wc_dict
    wc_dict    = {}
    if combination_type == 'comb1':
        wc_dict['C9_bsee'    ]            = param_dict['deltaC9']
        wc_dict['C10_bsee'   ]            = param_dict['deltaC10']
    elif combination_type == 'comb2':
        wc_dict['C9_bsee'    ]            = param_dict['deltaC9']  
        wc_dict['C9p_bsee'   ]            = param_dict['deltaC9p'] 
    elif combination_type == 'comb3':
        wc_dict['C9_bsee'    ]            = param_dict['deltaC9']  
        wc_dict['C10p_bsee'  ]            = param_dict['deltaC10p']
    elif combination_type == 'comb4':
        wc_dict['C10_bsee'   ]            = param_dict['deltaC10'] 
        wc_dict['C10p_bsee'  ]            = param_dict['deltaC10p']
    elif combination_type == 'comb5':
        wc_dict['C9p_bsee'   ]            = param_dict['deltaC9p'] 
        wc_dict['C10p_bsee'  ]            = param_dict['deltaC10p']
    elif combination_type == 'comb6':
        wc_dict['C9p_bsee'   ]            = param_dict['deltaC9p'] 
        wc_dict['C10_bsee'   ]            = param_dict['deltaC10'] 

    ##print the values
    #for k in list(wc_dict.keys()): print(k, wc_dict[k])
    wilson = Wilson(wc_dict, scale=4.8, eft='WET', basis='flavio')
    return -2. * likelihood.log_likelihood(flavio_default_params, wilson) 

def run_scipy_optimize(pars, cost_func, args=(), method=None, options=None):
    float_pars = [p for p in pars if p.floating()]

    def func(par, *args):
        for i, p in enumerate(float_pars): p.update(par[i])
        kwargs = {p.name: p() for p in float_pars}
        func.n += 1
        nll_val = cost_func(kwargs, args)
        if func.n % 20 == 0: print(func.n, nll_val, par)
        return nll_val

    func.n = 0
    start = [p.init_value for p in float_pars]
    limit = [(p.lower_limit, p.upper_limit) for p in float_pars]

    initlh = func(start, *args)
    starttime = timer()
    opt = minimize(func, start, args=args, method=method, bounds=limit, options=options)
    endtime = timer()

    results = {"params": {}}  # Get fit results and update parameters
    for n, p in enumerate(float_pars):
        p.update(opt.x[n])
        p.fitted_value = opt.x[n]
        p.error = 0.
        results["params"][p.name] = (p.fitted_value, p.error)

    at_limit=False
    for i, (lower, upper) in enumerate(limit):
        if lower and upper is not None:
             if np.isclose(lower,opt.x[i]) or np.isclose(upper,opt.x[i]):
                    print('Some parameters at limit')
                    at_limit=True

    has_reached_call_limit=False
    if options['maxiter'] is not None:
        maxiter=options['maxiter']
        if func.n == maxiter:
            has_reached_call_limit=True
                         
    results["covmatrix"] = -np.ones_like(opt.x) 
    #results["x"]=opt.x
    results["initlh"] = initlh
    results["loglh"] = opt.fun
    results["iterations"] = -1
    results["func_calls"] = func.n
    results["time"] = endtime - starttime
    results["is_valid"] = int(opt.success) 
    results["has_parameters_at_limit"] = int(at_limit)
    results["has_accurate_covar"] = -1
    results["has_posdef_covar"] = -1
    results["has_made_posdef_covar"] = -1
    results["has_reached_call_limit"] = int(has_reached_call_limit)
    return results

def func_op(par, *args):
    pars = args[2:]
    float_pars = [p for p in pars if p.floating()]
    for i, p in enumerate(float_pars): p.update(par[i])
    kwargs = {p.name: p() for p in float_pars}
    func_op.n += 1
    nll_val = cost_func_rx(kwargs, args) #has to be explicity set here
    return nll_val

#NB: Cannot do run_optimparallel(pars, cost_func, args=(), options=None) run into error
def run_optimparallel(pars, args=(), options=None):
    float_pars = [p for p in pars if p.floating()]
    func_op.n = 0
    start = [p.init_value for p in float_pars]
    limit = [(p.lower_limit, p.upper_limit) for p in float_pars]
    args_list=list(args)
    args_list+=pars
    args=tuple(args_list)
    initlh = func_op(start, *args)
    starttime = timer()
    parallel={'max_workers': 1, 'loginfo': True}
    opt = minimize_parallel(func_op, start, args=args,  bounds=limit, options=options, parallel = parallel)
    endtime = timer()

    results = {"params": {}}  # Get fit results and update parameters
    for n, p in enumerate(float_pars):
        p.update(opt.x[n])
        p.fitted_value = opt.x[n]
        p.error = np.sqrt(np.diag(opt.hess_inv.todense()))[n]
        results["params"][p.name] = (p.fitted_value, p.error)

    at_limit=False
    for i, (lower, upper) in enumerate(limit):
        if lower and upper is not None:
             if np.isclose(lower,opt.x[i]) or np.isclose(upper,opt.x[i]):
                    print('Some parameters at limit')
                    at_limit=True

    has_reached_call_limit=False
    if options['maxiter'] is not None:
        maxiter=options['maxiter']
        if opt.nit == maxiter:
            has_reached_call_limit=True


    results["covmatrix"] = opt.hess_inv.todense()
    #results["x"]=opt.x
    results["initlh"] = initlh
    results["loglh"] = opt.fun
    results["iterations"] = -1
    results["func_calls"] = func_op.n
    results["time"] = endtime - starttime
    results["is_valid"] = int(opt.success) 
    results["has_parameters_at_limit"] = int(at_limit)
    results["has_accurate_covar"] = -1
    results["has_posdef_covar"] = -1
    results["has_made_posdef_covar"] = -1
    results["has_reached_call_limit"] = int(has_reached_call_limit)
    #if identity fail fit
    if np.array_equal(results["covmatrix"], np.ones_like(opt.x)): results["is_valid"] = 0
    return results    

def func_op_lfuv(par, *args):
    pars = args[2:]
    float_pars = [p for p in pars if p.floating()]
    for i, p in enumerate(float_pars): p.update(par[i])
    kwargs = {p.name: p() for p in float_pars}
    func_op_lfuv.n += 1
    nll_val = cost_func_rx_lfuv(kwargs, args) #has to be explicity set here
    return nll_val

#NB: Cannot do run_optimparallel(pars, cost_func, args=(), options=None) run into error
def run_optimparallel_lfuv(pars, args=(), options=None):
    float_pars = [p for p in pars if p.floating()]
    func_op_lfuv.n = 0
    start = [p.init_value for p in float_pars]
    limit = [(p.lower_limit, p.upper_limit) for p in float_pars]
    args_list=list(args)
    args_list+=pars
    args=tuple(args_list)
    initlh = func_op_lfuv(start, *args)
    starttime = timer()
    parallel={'max_workers': 5, 'loginfo': True}
    opt = minimize_parallel(func_op, start, args=args,  bounds=limit, options=options, parallel = parallel)
    endtime = timer()

    results = {"params": {}}  # Get fit results and update parameters
    for n, p in enumerate(float_pars):
        p.update(opt.x[n])
        p.fitted_value = opt.x[n]
        p.error = np.sqrt(np.diag(opt.hess_inv.todense()))[n]
        results["params"][p.name] = (p.fitted_value, p.error)

    at_limit=False
    for i, (lower, upper) in enumerate(limit):
        if lower and upper is not None:
             if np.isclose(lower,opt.x[i]) or np.isclose(upper,opt.x[i]):
                    print('Some parameters at limit')
                    at_limit=True

    has_reached_call_limit=False
    if options['maxiter'] is not None:
        maxiter=options['maxiter']
        if opt.nit == maxiter:
            has_reached_call_limit=True


    results["covmatrix"] = opt.hess_inv.todense()
    #results["x"]=opt.x
    results["initlh"] = initlh
    results["loglh"] = opt.fun
    results["iterations"] = -1
    results["func_calls"] = func_op_lfuv.n
    results["time"] = endtime - starttime
    results["is_valid"] = int(opt.success) 
    results["has_parameters_at_limit"] = int(at_limit)
    results["has_accurate_covar"] = -1
    results["has_posdef_covar"] = -1
    results["has_made_posdef_covar"] = -1
    results["has_reached_call_limit"] = int(has_reached_call_limit)
    #if identity fail fit
    if np.array_equal(results["covmatrix"], np.ones_like(opt.x)): results["is_valid"] = 0
    return results    


class MinuitFunction(object):
    """Function wrapper for Minuit to allow supplying function with additional
    arguments"""
    def __init__(self, f, args=()):
        """Initialize the instance. f: function"""
        self.f = f
        self.args = args
        self.func_code = iminuit.util.make_func_code('x')

    def __call__(self, x):
        return self.f(x, *self.args)

def run_minuit(pars, cost_func, args=(), use_hesse = False, use_minos = False, get_covariance = False, print_level = 2):
    """
    Run IMinuit to minimise chisq function

    pars : list of FitParameters
    use_hesse : if True, uses HESSE for error estimation
    use_minos : if True, use MINOS for asymmetric error estimation
    get_covariance: if True, get the covariance matrix from the fit
    print_level: print level for minuit

    returns the dictionary with the values and errors of the fit parameters
    """

    float_pars = [p for p in pars if p.floating()]

    def func(par, *args):
        for i, p in enumerate(float_pars): p.update(par[i])
        kwargs = {p.name: p() for p in float_pars}
        func.n += 1
        nll_val = cost_func(kwargs, args)
        if func.n % 20 == 0: print(func.n, nll_val, par)
        return nll_val

    func.n = 0

    start = [p.init_value for p in float_pars]
    error = [p.step_size for p in float_pars]
    limit = [(p.lower_limit, p.upper_limit) for p in float_pars]
    name  = [p.name for p in float_pars]
    mfun  = MinuitFunction(f=func, args=args)

    minuit = Minuit(mfun,start,name=name)
    minuit.errordef=Minuit.LEAST_SQUARES
    minuit.print_level = print_level
    minuit.errors = error
    minuit.limits = limit
    #minuit.tol = 10.0 #0.002 * tol * errordef, defaul tol = 0.1

    initlh = func(start, *args)
    starttime = timer()
    mgrd = minuit.migrad(ncall=None, iterate=5)#default
    #mgrd = minuit.migrad(ncall=50000, iterate=5)
    #print(mgrd)
    if use_hesse:
        hesse = minuit.hesse()
        #print(hesse)

    if use_minos:
        mns = minuit.minos()
        #print(mns)

    endtime = timer()

    par_states = minuit.params
    f_min = minuit.fmin
    #print the nice tables of fit results
    print(f_min)
    print(par_states)
    #print(minuit.covariance.correlation())

    results = {"params": {}}  # Get fit results and update parameters
    for n, p in enumerate(float_pars):
        p.update(par_states[n].value)
        p.fitted_value = par_states[n].value
        p.error = par_states[n].error
        results["params"][p.name] = (p.fitted_value, p.error)

    # return fit results
    results["initlh"] = initlh
    results["loglh"] = f_min.fval
    results["iterations"] = f_min.nfcn
    results["func_calls"] = func.n
    results["time"] = endtime - starttime
    #results["x"]=list(mgrd.values)
    #is_valid == (has_valid_parameters & !has_reached_call_limit & !is_above_max_edm)
    results["is_valid"] = int(f_min.is_valid) 
    results["has_parameters_at_limit"] = int(f_min.has_parameters_at_limit)
    results["has_accurate_covar"] = int(f_min.has_accurate_covar)
    results["has_posdef_covar"] = int(f_min.has_posdef_covar)
    results["has_made_posdef_covar"] = int(f_min.has_made_posdef_covar)
    results["has_reached_call_limit"] = int(f_min.has_reached_call_limit)

    #store covariance matrix of parameters
    if get_covariance:
        covarmatrix = {}
        for p1 in float_pars:
            covarmatrix[p1.name] = {}
            for p2 in float_pars:
                covarmatrix[p1.name][p2.name] = minuit.covariance[p1.name, p2.name]

        results["covmatrix"] = covarmatrix

    #check is error is nan and force invalid minima (strange that it occurs maybe related to log(L) returning nan, maybe we need to enforce that).
    for k in results['params'].keys():
        if np.isnan(results['params'][k][1]):
            results["is_valid"] = 0
            return results


    return results

def randomise_params(tot_params):
    for k in tot_params.keys():
        p = tot_params[k]
        if p.floating():
            low     = -1.
            high    =  1.
            #low     = tot_params[k].lower_limit
            #high    = tot_params[k].upper_limit
            #new_val = np.random.uniform(tot_params[k].lower_limit, tot_params[k].upper_limit, size=1)
            #new_val = np.random.uniform(tot_params[k].lower_limit, tot_params[k].upper_limit, size=1)
            new_val = np.random.uniform(low, high, size=1)
            print('Randomising the parameter,', k, 'to new value', new_val[0])
            tot_params[k].update(new_val[0])
            tot_params[k].init_value = new_val[0]

def set_params_to(tot_params, x0):
    for k in tot_params.keys():
        p = tot_params[k]
        if p.floating():
            print('Setting the parameter,', k, 'to new value', x0[k][0])
            tot_params[k].update(x0[k][0])
            tot_params[k].init_value = x0[k][0]

def Minimize(tot_params, cost_func, args=(), methods=None, nfits = 1, use_hesse = False, use_minos = False, get_covariance = False, randomise = False, disp=False, **kwargs):
    reslts = None

    if methods is None:
        raise Exception('You have set no methods for minimisation!')

    for nfit in range(nfits):
        results = None
        for m in methods:
            numfits_valid_minimum   = 0
            valid_minimum = False
            while not valid_minimum:
                print("Starting try no. {} with method {} with a sub-try no. {}".format(nfit+1, m, numfits_valid_minimum+1))

                #randomise parameters b/w -1 and 1
                if randomise: randomise_params(tot_params)

                #randomise parameters b/w -1 and 1 if not valid_minimum
                if numfits_valid_minimum != 0 and not valid_minimum: 
                    if results['has_reached_call_limit']: 
                        set_params_to(tot_params, results["params"]) 
                    else:
                        randomise_params(tot_params)
                    
                #Conduct the fit
                if m=='MIGRAD':
                    results = run_minuit(list(tot_params.values()), cost_func, args=args, use_hesse = use_hesse, use_minos = use_minos, get_covariance = get_covariance, print_level=1) 
                elif m in ['SLSQP', 'Parallel', 'Parallel-LFUV', 'BFGS', 'Nelder-Mead']:
                    options = {'disp': disp, 'maxiter': 150}
                    if m == 'Parallel':
                        print('The spcified cost_func will be ignored the cost_func_rx is hard coded')
                        results = run_optimparallel(list(tot_params.values()), args=args, options=options)
                    elif m == 'Parallel-LFUV':
                        print('The spcified cost_func will be ignored the cost_func_rx_lfuv is hard coded')
                        results = run_optimparallel_lfuv(list(tot_params.values()), args=args, options=options)
                    else:
                        results = run_scipy_optimize(list(tot_params.values()), cost_func, args=args, method=m, options=options)

                #check if valid fit or not
                if results['is_valid'] != 1:
                    if numfits_valid_minimum == 2:
                        print('Minimizer {0} fails after three tries.'.format(m))
                        if results['has_reached_call_limit']: 
                            print('Since the call limit was reached, setting values to the result of previous fit!')
                            set_params_to(tot_params, results["params"]) 

                        print(results)
                        break #breaks the 1st while loop for finding valid minima goes to next minimizer

                    numfits_valid_minimum   += 1
                    continue
                else:
                    valid_minimum = True #breaks the 1st while loop for finding valid minima
                    results['minimizer']=m

            if valid_minimum: break #breaks the 2nd minimizer loop 

        #out of nfits pick the result with the least negative log likelihood (NLL)
        if nfit == 0: 
            nllval = results['loglh']
            reslts = results
        else:
            if nllval > results['loglh']:
                nllval = results['loglh']
                reslts = results

    return reslts

#def write_fit_results(results, tot_params, filename, store_covariance = False):
#    f = open(filename, "w")
#    floated_params = [tot_params[k] for k in tot_params.keys() if tot_params[k].floating()]
#    for p in floated_params:
#        s = "%s " % p.name
#        for i in results["params"][p.name]:
#            s += "%f " % i
#
#        f.write(s + "\n")
#
#    s = "loglh %f\n"                   % (results["loglh"])
#    #see here to understand what below mean: https://iminuit.readthedocs.io/en/stable/reference.html
#    #is_valid == (has_valid_parameters & !has_reached_call_limit & !is_above_max_edm)
#    s+= "is_valid %i\n"                % (results["is_valid"])
#    s+= "has_parameters_at_limit %i\n" % (results["has_parameters_at_limit"])
#    s+= "has_accurate_covar %i\n"      % (results["has_accurate_covar"])
#    s+= "has_posdef_covar %i\n"        % (results["has_posdef_covar"])
#    s+= "has_made_posdef_covar %i"   % (results["has_made_posdef_covar"])
#    f.write(s + "\n")
#    f.close()
#
#    if store_covariance:
#        f = open(filename.replace('.txt', '_covmatrix.txt'), "w")
#        for k1 in list(results["covmatrix"].keys()):
#            for k2 in list(results["covmatrix"].keys()):
#                s = '{0} {1} {2}'.format(k1, k2, results["covmatrix"][k1][k2])
#                f.write(s + "\n")
#        f.close()
